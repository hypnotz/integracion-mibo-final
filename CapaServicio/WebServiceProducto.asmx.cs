﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceMantenedorProductos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceProducto : System.Web.Services.WebService
    {

        [WebMethod]
        public void insertarProductoService(Productos producto)
        {
            NegocioProducto auxNegocio = new NegocioProducto();

            auxNegocio.insertarProducto(producto);

        }

        [WebMethod]
        public void eliminarProductoService(int id_Producto)
        {
            NegocioProducto auxNegocio = new NegocioProducto();

            auxNegocio.eliminarProducto(id_Producto);

        }

        [WebMethod]
        public void actualizarProductoService(Productos producto)
        {
            NegocioProducto auxNegocio = new NegocioProducto();

            auxNegocio.actualizarProducto(producto);

        }


        [WebMethod]
        public DataSet retornaProductosService()
        {
            NegocioProducto auxNegocio = new NegocioProducto();

            return auxNegocio.retornaProductos();

        }

        [WebMethod]
        public DataTable obtenerProductosCboService()
        {
            NegocioProducto auxNegocio = new NegocioProducto();

            return auxNegocio.obtenerProductosCbo();

        }
        [WebMethod]
        public DataTable retornaIdMaximoProducto()
        {
            NegocioProducto auxNegocio = new NegocioProducto();

            return auxNegocio.retornaIdMaximoProducto();
        }
    }
}
