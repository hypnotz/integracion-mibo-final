﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceEntradaProducto
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceEntradaProducto : System.Web.Services.WebService
    {

        [WebMethod]
        public DataTable retornaIdMaximoService()
        {
            NegocioEntradaProducto auxEntradaProducto = new NegocioEntradaProducto();

            return auxEntradaProducto.retornaIdMaximo();
        }

        [WebMethod]
        public void insertarEntradaCabeceraService(EntradaCabecera entradaCabecera)
        {
            NegocioEntradaProducto auxEntradaProducto = new NegocioEntradaProducto();

            auxEntradaProducto.insertarEntradaCabecera(entradaCabecera);
        }
    }
}
