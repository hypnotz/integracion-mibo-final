﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceProveedor
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceProveedor : System.Web.Services.WebService
    {

        [WebMethod]
        public void insertarProveedorService(Proveedor proveedor)
        {
            NegocioProveedor auxNegocio = new NegocioProveedor();
            auxNegocio.insertarProveedor(proveedor);
        }
        [WebMethod]
        public void eliminarProveedorService(String rut_proveedor)
        {
            NegocioProveedor auxNegocio = new NegocioProveedor();
            auxNegocio.eliminarProveedor(rut_proveedor);
        }
        [WebMethod]
        public void actualizarProveedorService(Proveedor proveedor)
        {
            NegocioProveedor auxNegocio = new NegocioProveedor();
            auxNegocio.actualizarProveedor(proveedor);
        }
        [WebMethod]
        public DataSet retornaProveedorService()
        {
            NegocioProveedor auxNegocio = new NegocioProveedor();
            return auxNegocio.retornaProveedor();
        }

        [WebMethod]
        public DataTable obtenerProveedorCboService()
        {
            NegocioProveedor auxNegocio = new NegocioProveedor();
            return auxNegocio.obtenerProveedorCbo();
        }
        [WebMethod]
        public DataSet retornaProveedorPorRut(Proveedor proveedor)
        {
            NegocioProveedor auxNegocio = new NegocioProveedor();

            return auxNegocio.retornaProveedorPorRut(proveedor);
        }
    }
}
