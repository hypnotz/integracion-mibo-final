﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceCategoria
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceCategoria : System.Web.Services.WebService
    {

        [WebMethod]
        public DataTable obtenerCategoriaService()
        {
            NegocioCategoria auxNegocio = new NegocioCategoria();

            return auxNegocio.obtenerCategoria();
        }

        [WebMethod]
        public void insertarCategoriaService(Categoria categoria)
        {
            NegocioCategoria auxNegocio = new NegocioCategoria();
            auxNegocio.insertarCategoria(categoria);
        }

        [WebMethod]
        public void eliminarCategoriaService(int id_categoria)
        {
            NegocioCategoria auxNegocio = new NegocioCategoria();
            auxNegocio.eliminarCategoria(id_categoria);
        }
        [WebMethod]
        public void actualizarProductoService(Categoria categoria)
        {
            NegocioCategoria auxNegocio = new NegocioCategoria();
            auxNegocio.actualizarProducto(categoria);
        }
        [WebMethod]
        public DataSet retornaCategoriaService()
        {
            NegocioCategoria auxNegocio = new NegocioCategoria();
            return auxNegocio.retornaCategoria();
        }
    }

 

}
