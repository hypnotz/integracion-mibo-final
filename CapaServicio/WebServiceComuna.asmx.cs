﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using CapaDTO;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceComuna
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceComuna : System.Web.Services.WebService
    {

        [WebMethod]
        public DataTable obtenerComunaService()
        {
            NegocioComuna auxNegocio = new NegocioComuna();

            return auxNegocio.obtenerComuna();
        }
    }
}
