﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceMantenedorRecursosHumanos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceRecursosHumanos : System.Web.Services.WebService
    {

        [WebMethod]
        public void insertarTrabajadorService(RecursosHumanos rh)
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            auxNegocio.insertarTrabajador(rh);
        }

        [WebMethod]
        public void actualizarTrabajadorService(RecursosHumanos recursosHumanos)
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            auxNegocio.actualizarTrabajador(recursosHumanos);
        }

        [WebMethod]
        public void eliminarTrabajadorService(String rut_empleado)
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            auxNegocio.eliminarTrabajador(rut_empleado);
        }
        [WebMethod]
        public DataSet retornaTrabajadorService()
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            return auxNegocio.retornaTrabajador();
        }
        [WebMethod]
        public DataTable obtenerTrabajadorCboService()
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            return auxNegocio.obtenerTrabajadorCbo();
        }
        [WebMethod]
        public DataSet retornaTrabajadorPorRutService(RecursosHumanos recursosHumanos)
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            return auxNegocio.retornaTrabajadorPorRut(recursosHumanos);
        }

        [WebMethod]
        public DataSet existeRutTest(String rut)
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            return auxNegocio.existeRutTest(rut);
        }
        [WebMethod]
        public DataTable consultaLogin(String rut_empleado, String contrasena)
        {
            NegocioRecursosHumanos auxNegocio = new NegocioRecursosHumanos();

            return auxNegocio.consultaLogin(rut_empleado,contrasena);
        }
    }
}
