﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceStockProductos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceStockProducto : System.Web.Services.WebService
    {

        [WebMethod]
        public DataSet retornaStockGeneralService()
        {
            NegocioStockProducto auxNegocioStock = new NegocioStockProducto();

            return auxNegocioStock.retornaStockGeneral();
        }

        [WebMethod]
        public DataSet retornaStockPorIDService(Stock stock)
        {
            NegocioStockProducto auxNegocioStock = new NegocioStockProducto();

            return auxNegocioStock.retornaStockPorID(stock);
        }

        [WebMethod]

        public void actualizarStock(Stock nuevoStock)
        {
            NegocioStockProducto auxNegocioStock = new NegocioStockProducto();

             auxNegocioStock.actualizarStock(nuevoStock);
        }
        [WebMethod]
        public void insertarProductoStock(Stock nuevoStock)
        {
            NegocioStockProducto auxNegocioStock = new NegocioStockProducto();

            auxNegocioStock.insertarProductoStock(nuevoStock);


        }
        [WebMethod]
        public DataSet retornaStockProductoPorID(Stock stock)
        {
            NegocioStockProducto auxNegocioStock = new NegocioStockProducto();

            return auxNegocioStock.retornaStockPorID(stock);
        }
        [WebMethod]
        public DataSet retornaStockProductoPorNombre(Productos productos)
        {
            
        NegocioStockProducto auxNegocioStock = new NegocioStockProducto();

            return auxNegocioStock.retornaStockProductoPorNombre(productos);
     }
   }
}
