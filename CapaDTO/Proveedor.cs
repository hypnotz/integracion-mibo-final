﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Proveedor
    {

        
        private int idProveedor;
        private string rutProveedor;
        private string nombreProveedor;
        private string direccion;
        private int telefono;
        private string email;
        private int idComuna;

        public int IdProveedor { get => idProveedor; set => idProveedor = value; }
        public string RutProveedor { get => rutProveedor; set => rutProveedor = value; }
        public string NombreProveedor { get => nombreProveedor; set => nombreProveedor = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public int Telefono { get => telefono; set => telefono = value; }
        public string Email { get => email; set => email = value; }
        public int IdComuna { get => idComuna; set => idComuna = value; }
    }
}
