﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class AjusteInventarioDetalle
    {

        private int idAjuste;
        private string motivo;
        private int idTipoAjuste;
        private int idDetalleInventario;
        private int idProducto;

        public int IdAjuste { get => idAjuste; set => idAjuste = value; }
        public string Motivo { get => motivo; set => motivo = value; }
        public int IdTipoAjuste { get => idTipoAjuste; set => idTipoAjuste = value; }
        public int IdDetalleInventario { get => idDetalleInventario; set => idDetalleInventario = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
    }
}
