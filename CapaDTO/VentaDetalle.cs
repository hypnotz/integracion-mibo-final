﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class VentaDetalle
    {
        private int idDetalleVenta;
        private int idProducto;
        private DateTime fecha;
        private int cantidadVenta;
        private int precio;
        private int subTotal;
        private int totalPagar;
        private int idVentaCabecera;

        public int IdDetalleVenta { get => idDetalleVenta; set => idDetalleVenta = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public int CantidadVenta { get => cantidadVenta; set => cantidadVenta = value; }
        public int Precio { get => precio; set => precio = value; }
        public int SubTotal { get => subTotal; set => subTotal = value; }
        public int TotalPagar { get => totalPagar; set => totalPagar = value; }
        public int IdVentaCabecera { get => idVentaCabecera; set => idVentaCabecera = value; }
    }
}
