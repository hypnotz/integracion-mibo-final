﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Stock
    {
        private int idDeposito;
        private int cantidad;
        private int idBodega;
        private int idProducto;

        public int IdDeposito { get => idDeposito; set => idDeposito = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int IdBodega { get => idBodega; set => idBodega = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }


    }
}