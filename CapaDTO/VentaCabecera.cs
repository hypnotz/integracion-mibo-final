﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class VentaCabecera
    {
        private int idVentaCabecera;
        private DateTime fecha;
        private string rutEmpleado;

        public int IdVentaCabecera { get => idVentaCabecera; set => idVentaCabecera = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string RutEmpleado { get => rutEmpleado; set => rutEmpleado = value; }
    }
}
