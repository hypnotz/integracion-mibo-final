﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class TipoAjuste
    {


        private int idTipoAjuste;
        private string nombre;
        private string descripcion;

        public int IdTipoAjuste { get => idTipoAjuste; set => idTipoAjuste = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }


    }
}
