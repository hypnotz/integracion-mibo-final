﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class TomaInventarioCabecera
    {
        private int idInventarioCabecera;
        private DateTime fecha;
        private string rutEmpleado;

        public int IdInventarioCabecera { get => idInventarioCabecera; set => idInventarioCabecera = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string RutEmpleado { get => rutEmpleado; set => rutEmpleado = value; }

    }
}
