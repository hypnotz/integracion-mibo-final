﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class EntradaCabecera
    {
        private int idEntrada;
        private DateTime fecha;
        private int idBodega;
        private int idProveedor;
        private string rutEmpleado;

        public int IdEntrada { get => idEntrada; set => idEntrada = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public int IdBodega { get => idBodega; set => idBodega = value; }
        public int IdProveedor { get => idProveedor; set => idProveedor = value; }
        public string RutEmpleado { get => rutEmpleado; set => rutEmpleado = value; }

    }
}


