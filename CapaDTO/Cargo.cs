﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Cargo
    {
        private int idCargo;
        private string nombre;
        private string descripcion;

        public int IdCargo { get => idCargo; set => idCargo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }

    }
}
