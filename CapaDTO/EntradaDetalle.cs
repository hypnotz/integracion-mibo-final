﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class EntradaDetalle
    {
        private int idDetalle;
        private int idProducto;
        private DateTime fecha;
        private int cantidad;
        private int precio;
        private int subTotal;
        private int idEntrada;

        public int IdDetalle { get => idDetalle; set => idDetalle = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int Precio { get => precio; set => precio = value; }
        public int SubTotal { get => subTotal; set => subTotal = value; }
        public int IdEntrada { get => idEntrada; set => idEntrada = value; }


    }
}
