﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Productos
    {


        private int idProducto;
        private string nombreProducto;
        private int precio;
        private int idCategoria;

        public int IdProducto { get => idProducto; set => idProducto = value; }
        public string NombreProducto { get => nombreProducto; set => nombreProducto = value; }
        public int Precio { get => precio; set => precio = value; }
        public int IdCategoria { get => idCategoria; set => idCategoria = value; }
    }
}
