﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Categoria
    {
        private int idCategoria;
        private string nombre;
        private string descripcionCategoria;

        public int IdCategoria { get => idCategoria; set => idCategoria = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string DescripcionCategoria { get => descripcionCategoria; set => descripcionCategoria = value; }


    }
}