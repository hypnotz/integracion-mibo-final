﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class TomaInventarioDetalle
    {
        private int idDetalleInventario;
        private int idProducto;
        private DateTime fecha;
        private int cantidadProducto;
        private int idInventarioCabecera;

        public int IdDetalleInventario { get => idDetalleInventario; set => idDetalleInventario = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public int CantidadProducto { get => cantidadProducto; set => cantidadProducto = value; }
        public int IdInventarioCabecera { get => idInventarioCabecera; set => idInventarioCabecera = value; }
    }
}
