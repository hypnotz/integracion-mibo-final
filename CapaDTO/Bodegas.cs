﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Bodegas
    {
        private int idBodega;
        private string nombreBodega;

        public int IdBodega { get => idBodega; set => idBodega = value; }
        public string NombreBodega { get => nombreBodega; set => nombreBodega = value; }
    }
}
    