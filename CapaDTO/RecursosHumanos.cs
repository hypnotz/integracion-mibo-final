﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class RecursosHumanos
    {
        private string rutEmpleado;
        private string nombre;
        private string apellido;
        private int telefono;
        private int idCargo;
        private string contrasena;

        public string RutEmpleado { get => rutEmpleado; set => rutEmpleado = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int Telefono { get => telefono; set => telefono = value; }
        public int IdCargo { get => idCargo; set => idCargo = value; }
        public string Contrasena { get => contrasena; set => contrasena = value; }
    }
}
