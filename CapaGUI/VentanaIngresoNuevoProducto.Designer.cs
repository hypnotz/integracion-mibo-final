﻿namespace CapaGUI
{
    partial class VentanaIngresoNuevoProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtIdProducto = new System.Windows.Forms.TextBox();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.txtNombreProducto = new System.Windows.Forms.TextBox();
            this.cboCategoria = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblProducto = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgProductos = new System.Windows.Forms.DataGridView();
            this.ID_PRODUCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMBRE_PRODUCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRECIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_CATEGORIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btoVolver = new System.Windows.Forms.Button();
            this.btoEliminar = new System.Windows.Forms.Button();
            this.btoListar = new System.Windows.Forms.Button();
            this.btoModificar = new System.Windows.Forms.Button();
            this.btoIngresar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(394, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Precio producto :";
            // 
            // txtIdProducto
            // 
            this.txtIdProducto.Location = new System.Drawing.Point(144, 29);
            this.txtIdProducto.Name = "txtIdProducto";
            this.txtIdProducto.Size = new System.Drawing.Size(201, 20);
            this.txtIdProducto.TabIndex = 43;
            this.txtIdProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIdProducto_KeyPress);
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(509, 25);
            this.txtPrecio.MaxLength = 8;
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(206, 20);
            this.txtPrecio.TabIndex = 42;
            this.txtPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_KeyPress);
            // 
            // txtNombreProducto
            // 
            this.txtNombreProducto.Location = new System.Drawing.Point(144, 81);
            this.txtNombreProducto.Name = "txtNombreProducto";
            this.txtNombreProducto.Size = new System.Drawing.Size(206, 20);
            this.txtNombreProducto.TabIndex = 41;
            // 
            // cboCategoria
            // 
            this.cboCategoria.DisplayMember = "-1";
            this.cboCategoria.FormattingEnabled = true;
            this.cboCategoria.Location = new System.Drawing.Point(509, 84);
            this.cboCategoria.Name = "cboCategoria";
            this.cboCategoria.Size = new System.Drawing.Size(203, 21);
            this.cboCategoria.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(376, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Categoría Producto :";
            // 
            // lblProducto
            // 
            this.lblProducto.AutoSize = true;
            this.lblProducto.Location = new System.Drawing.Point(28, 32);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(71, 13);
            this.lblProducto.TabIndex = 37;
            this.lblProducto.Text = "Id Producto : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Nombre Producto :";
            // 
            // dgProductos
            // 
            this.dgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_PRODUCTO,
            this.NOMBRE_PRODUCTO,
            this.PRECIO,
            this.ID_CATEGORIA});
            this.dgProductos.Location = new System.Drawing.Point(24, 157);
            this.dgProductos.Name = "dgProductos";
            this.dgProductos.Size = new System.Drawing.Size(745, 170);
            this.dgProductos.TabIndex = 35;
            // 
            // ID_PRODUCTO
            // 
            this.ID_PRODUCTO.DataPropertyName = "ID_PRODUCTO";
            this.ID_PRODUCTO.HeaderText = "ID";
            this.ID_PRODUCTO.Name = "ID_PRODUCTO";
            this.ID_PRODUCTO.Width = 52;
            // 
            // NOMBRE_PRODUCTO
            // 
            this.NOMBRE_PRODUCTO.DataPropertyName = "NOMBRE_PRODUCTO";
            this.NOMBRE_PRODUCTO.HeaderText = "NOMBRE_PRODUCTO";
            this.NOMBRE_PRODUCTO.Name = "NOMBRE_PRODUCTO";
            this.NOMBRE_PRODUCTO.Width = 250;
            // 
            // PRECIO
            // 
            this.PRECIO.DataPropertyName = "PRECIO";
            this.PRECIO.HeaderText = "PRECIO";
            this.PRECIO.Name = "PRECIO";
            this.PRECIO.Width = 190;
            // 
            // ID_CATEGORIA
            // 
            this.ID_CATEGORIA.DataPropertyName = "ID_CATEGORIA";
            this.ID_CATEGORIA.HeaderText = "ID_CATEGORIA";
            this.ID_CATEGORIA.Name = "ID_CATEGORIA";
            this.ID_CATEGORIA.Width = 210;
            // 
            // btoVolver
            // 
            this.btoVolver.Location = new System.Drawing.Point(565, 399);
            this.btoVolver.Name = "btoVolver";
            this.btoVolver.Size = new System.Drawing.Size(150, 23);
            this.btoVolver.TabIndex = 34;
            this.btoVolver.Text = "Volver al Menú";
            this.btoVolver.UseVisualStyleBackColor = true;
            this.btoVolver.Click += new System.EventHandler(this.btoVolver_Click);
            // 
            // btoEliminar
            // 
            this.btoEliminar.Location = new System.Drawing.Point(308, 399);
            this.btoEliminar.Name = "btoEliminar";
            this.btoEliminar.Size = new System.Drawing.Size(158, 23);
            this.btoEliminar.TabIndex = 33;
            this.btoEliminar.Text = "Eliminar Producto";
            this.btoEliminar.UseVisualStyleBackColor = true;
            this.btoEliminar.Click += new System.EventHandler(this.btoEliminar_Click);
            // 
            // btoListar
            // 
            this.btoListar.Location = new System.Drawing.Point(47, 399);
            this.btoListar.Name = "btoListar";
            this.btoListar.Size = new System.Drawing.Size(150, 23);
            this.btoListar.TabIndex = 32;
            this.btoListar.Text = "Listar todos los Productos";
            this.btoListar.UseVisualStyleBackColor = true;
            this.btoListar.Click += new System.EventHandler(this.btoListar_Click);
            // 
            // btoModificar
            // 
            this.btoModificar.Location = new System.Drawing.Point(308, 347);
            this.btoModificar.Name = "btoModificar";
            this.btoModificar.Size = new System.Drawing.Size(150, 23);
            this.btoModificar.TabIndex = 31;
            this.btoModificar.Text = "Modificar Producto";
            this.btoModificar.UseVisualStyleBackColor = true;
            this.btoModificar.Click += new System.EventHandler(this.btoModificar_Click);
            // 
            // btoIngresar
            // 
            this.btoIngresar.Location = new System.Drawing.Point(47, 347);
            this.btoIngresar.Name = "btoIngresar";
            this.btoIngresar.Size = new System.Drawing.Size(150, 23);
            this.btoIngresar.TabIndex = 30;
            this.btoIngresar.Text = "Ingresar Nuevo Producto";
            this.btoIngresar.UseVisualStyleBackColor = true;
            this.btoIngresar.Click += new System.EventHandler(this.btoIngresar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(562, 347);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(150, 23);
            this.btnBuscar.TabIndex = 46;
            this.btnBuscar.Text = "Buscar Producto";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // VentanaIngresoNuevoProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 450);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIdProducto);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.txtNombreProducto);
            this.Controls.Add(this.cboCategoria);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblProducto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgProductos);
            this.Controls.Add(this.btoVolver);
            this.Controls.Add(this.btoEliminar);
            this.Controls.Add(this.btoListar);
            this.Controls.Add(this.btoModificar);
            this.Controls.Add(this.btoIngresar);
            this.Name = "VentanaIngresoNuevoProducto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentanaIngresoNuevoProducto";
            this.Load += new System.EventHandler(this.VentanaIngresoNuevoProducto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIdProducto;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.TextBox txtNombreProducto;
        private System.Windows.Forms.ComboBox cboCategoria;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblProducto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgProductos;
        private System.Windows.Forms.Button btoVolver;
        private System.Windows.Forms.Button btoEliminar;
        private System.Windows.Forms.Button btoListar;
        private System.Windows.Forms.Button btoModificar;
        private System.Windows.Forms.Button btoIngresar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_PRODUCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMBRE_PRODUCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRECIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_CATEGORIA;
        private System.Windows.Forms.Button btnBuscar;
    }
}