﻿namespace CapaGUI
{
    partial class VentanaEntradaProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgEntrada = new System.Windows.Forms.DataGridView();
            this.idDetalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idEntrada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboNombreProducto = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboNombreTrabajador = new System.Windows.Forms.ComboBox();
            this.cboNombreBodega = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblIdEntradaActual = new System.Windows.Forms.Label();
            this.lblIdActual = new System.Windows.Forms.Label();
            this.cboNombreProveedor = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btoVolver = new System.Windows.Forms.Button();
            this.btoIngresarEntrada = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgEntrada)).BeginInit();
            this.SuspendLayout();
            // 
            // dgEntrada
            // 
            this.dgEntrada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEntrada.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDetalle,
            this.idProducto,
            this.Fecha,
            this.Cantidad,
            this.Precio,
            this.SubTotal,
            this.idEntrada});
            this.dgEntrada.Location = new System.Drawing.Point(37, 216);
            this.dgEntrada.Name = "dgEntrada";
            this.dgEntrada.Size = new System.Drawing.Size(752, 152);
            this.dgEntrada.TabIndex = 111;
            // 
            // idDetalle
            // 
            this.idDetalle.HeaderText = "idDetalle";
            this.idDetalle.Name = "idDetalle";
            // 
            // idProducto
            // 
            this.idProducto.HeaderText = "idProducto";
            this.idProducto.Name = "idProducto";
            // 
            // Fecha
            // 
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            // 
            // SubTotal
            // 
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            // 
            // idEntrada
            // 
            this.idEntrada.HeaderText = "idEntrada";
            this.idEntrada.Name = "idEntrada";
            // 
            // cboNombreProducto
            // 
            this.cboNombreProducto.FormattingEnabled = true;
            this.cboNombreProducto.Location = new System.Drawing.Point(154, 128);
            this.cboNombreProducto.Name = "cboNombreProducto";
            this.cboNombreProducto.Size = new System.Drawing.Size(149, 21);
            this.cboNombreProducto.TabIndex = 110;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 109;
            this.label2.Text = "Nombre Producto  :";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(477, 93);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(141, 20);
            this.txtPrecio.TabIndex = 108;
            this.txtPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(345, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 107;
            this.label5.Text = "Precio  :";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(154, 174);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(149, 20);
            this.txtCantidad.TabIndex = 106;
            this.txtCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 177);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 105;
            this.label9.Text = "Cantidad  :";
            // 
            // cboNombreTrabajador
            // 
            this.cboNombreTrabajador.FormattingEnabled = true;
            this.cboNombreTrabajador.Location = new System.Drawing.Point(477, 42);
            this.cboNombreTrabajador.Name = "cboNombreTrabajador";
            this.cboNombreTrabajador.Size = new System.Drawing.Size(141, 21);
            this.cboNombreTrabajador.TabIndex = 104;
            // 
            // cboNombreBodega
            // 
            this.cboNombreBodega.FormattingEnabled = true;
            this.cboNombreBodega.Location = new System.Drawing.Point(154, 90);
            this.cboNombreBodega.Name = "cboNombreBodega";
            this.cboNombreBodega.Size = new System.Drawing.Size(149, 21);
            this.cboNombreBodega.TabIndex = 100;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 99;
            this.label7.Text = "Nombre Bodega :";
            // 
            // lblIdEntradaActual
            // 
            this.lblIdEntradaActual.AutoSize = true;
            this.lblIdEntradaActual.Location = new System.Drawing.Point(741, 11);
            this.lblIdEntradaActual.Name = "lblIdEntradaActual";
            this.lblIdEntradaActual.Size = new System.Drawing.Size(0, 13);
            this.lblIdEntradaActual.TabIndex = 98;
            // 
            // lblIdActual
            // 
            this.lblIdActual.AutoSize = true;
            this.lblIdActual.Location = new System.Drawing.Point(588, 11);
            this.lblIdActual.Name = "lblIdActual";
            this.lblIdActual.Size = new System.Drawing.Size(49, 13);
            this.lblIdActual.TabIndex = 97;
            this.lblIdActual.Text = "Id Actual";
            // 
            // cboNombreProveedor
            // 
            this.cboNombreProveedor.FormattingEnabled = true;
            this.cboNombreProveedor.Location = new System.Drawing.Point(154, 47);
            this.cboNombreProveedor.Name = "cboNombreProveedor";
            this.cboNombreProveedor.Size = new System.Drawing.Size(149, 21);
            this.cboNombreProveedor.TabIndex = 96;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 95;
            this.label4.Text = "Nombre Proveedor :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(344, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 94;
            this.label3.Text = "Nombre Trabajador  :";
            // 
            // btoVolver
            // 
            this.btoVolver.Location = new System.Drawing.Point(614, 428);
            this.btoVolver.Name = "btoVolver";
            this.btoVolver.Size = new System.Drawing.Size(150, 23);
            this.btoVolver.TabIndex = 93;
            this.btoVolver.Text = "Volver al Menú";
            this.btoVolver.UseVisualStyleBackColor = true;
            this.btoVolver.Click += new System.EventHandler(this.btoVolver_Click);
            // 
            // btoIngresarEntrada
            // 
            this.btoIngresarEntrada.Location = new System.Drawing.Point(269, 406);
            this.btoIngresarEntrada.Name = "btoIngresarEntrada";
            this.btoIngresarEntrada.Size = new System.Drawing.Size(166, 23);
            this.btoIngresarEntrada.TabIndex = 92;
            this.btoIngresarEntrada.Text = "Ingresar Entrada Producto";
            this.btoIngresarEntrada.UseVisualStyleBackColor = true;
            this.btoIngresarEntrada.Click += new System.EventHandler(this.btoIngresarEntrada_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(614, 385);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(150, 23);
            this.btnLimpiar.TabIndex = 113;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            // 
            // VentanaEntradaProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 466);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.dgEntrada);
            this.Controls.Add(this.cboNombreProducto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cboNombreTrabajador);
            this.Controls.Add(this.cboNombreBodega);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblIdEntradaActual);
            this.Controls.Add(this.lblIdActual);
            this.Controls.Add(this.cboNombreProveedor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btoVolver);
            this.Controls.Add(this.btoIngresarEntrada);
            this.Name = "VentanaEntradaProducto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "+";
            this.Load += new System.EventHandler(this.VentanaEntradaProducto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgEntrada)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgEntrada;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDetalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEntrada;
        private System.Windows.Forms.ComboBox cboNombreProducto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboNombreTrabajador;
        private System.Windows.Forms.ComboBox cboNombreBodega;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblIdEntradaActual;
        private System.Windows.Forms.Label lblIdActual;
        private System.Windows.Forms.ComboBox cboNombreProveedor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btoVolver;
        private System.Windows.Forms.Button btoIngresarEntrada;
        private System.Windows.Forms.Button btnLimpiar;
    }
}