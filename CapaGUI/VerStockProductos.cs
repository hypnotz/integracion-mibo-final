﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaDTO;

namespace CapaGUI
{
    public partial class VerStockProductos : Form
    {
        public VerStockProductos()
        {
            InitializeComponent();
            grillaProducto.AutoGenerateColumns = false;
        }

        private void VerStockProductos_Load(object sender, EventArgs e)
        {
            //ServiceStockProducto.WebServiceStockProductoSoapClient auxNegocioProducto = new ServiceStockProducto.WebServiceStockProductoSoapClient();
            //cboNombre.DisplayMember = "NOMBRE_PRODUCTO";
            //cboNombre.ValueMember = "ID_PRODUCTO";
            //cboNombre.DataSource = auxNegocioProducto.obtenerProductosCboService();
            //auxNegocioProducto.obtenerProductosCboService();

            ServiceStockProducto.WebServiceStockProductoSoapClient auxNegocioStock = new ServiceStockProducto.WebServiceStockProductoSoapClient();
            this.grillaProducto.DataSource = auxNegocioStock.retornaStockGeneralService();
            this.grillaProducto.DataMember = "Stock";
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            VentanaAdministrador pAdministrador = new VentanaAdministrador();
            this.Hide();
            pAdministrador.ShowDialog();
            this.Close();
        }

        private void btnBuscarID_Click(object sender, EventArgs e)
        {

            try
            {
                ServiceStockProducto.WebServiceStockProductoSoapClient auxNegocioStock = new ServiceStockProducto.WebServiceStockProductoSoapClient();
                ServiceStockProducto.Stock auxStock = new ServiceStockProducto.Stock();

                auxStock.IdProducto = Convert.ToInt32(txtIdProducto.Text);
                auxNegocioStock.retornaStockProductoPorID(auxStock);

                grillaProducto.DataSource = auxNegocioStock.retornaStockProductoPorID(auxStock);
                this.grillaProducto.DataMember = "Stock";
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error " + ex.Message);
            }  
        }

        private void btnBuscarNombre_Click(object sender, EventArgs e)
        {
            //NegocioStockProducto auxNegocioStock = new NegocioStockProducto();
            //Productos auxProducto = new Productos();

            ServiceStockProducto.WebServiceStockProductoSoapClient auxNegocioStock = new ServiceStockProducto.WebServiceStockProductoSoapClient();
            ServiceStockProducto.Productos auxProducto = new ServiceStockProducto.Productos();

            auxProducto.NombreProducto = txtBuscaNombre.Text;
            auxNegocioStock.retornaStockProductoPorNombre(auxProducto);

            grillaProducto.DataSource = auxNegocioStock.retornaStockProductoPorNombre(auxProducto);
            this.grillaProducto.DataMember = "Stock";
        }

        private void btnListarTodos_Click(object sender, EventArgs e)
        {
            ServiceStockProducto.WebServiceStockProductoSoapClient auxNegocioStock = new ServiceStockProducto.WebServiceStockProductoSoapClient();
            this.grillaProducto.DataSource = auxNegocioStock.retornaStockGeneralService();
            this.grillaProducto.DataMember = "Stock";
        }

        private void txtIdProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
    }
}
