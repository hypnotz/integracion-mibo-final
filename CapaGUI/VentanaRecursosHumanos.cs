﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUI
{
    public partial class VentanaRecursosHumanos : Form
    {
        public VentanaRecursosHumanos()
        {
            InitializeComponent();
            grillaTrabajadores.AutoGenerateColumns = false;
        }

        private void VentanaRecursosHumanos_Load(object sender, EventArgs e)
        {

            //cboCargo.Items.Add("Seleccione");

            txtRut.Focus();
            cboCargo.DropDownStyle = ComboBoxStyle.DropDownList;
            try
            {
                actualizarGrilla();
                NegocioCargo auxNegocioCargo = new NegocioCargo();
                cboCargo.DisplayMember = "NOMBRE";
                cboCargo.ValueMember = "ID_CARGO";
                cboCargo.DataSource = auxNegocioCargo.obtenerCargo();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void actualizarGrilla()
        {
            try
            {
                ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient auxNegocioRecursos = new ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient();
                this.grillaTrabajadores.DataSource = auxNegocioRecursos.retornaTrabajadorService();
                this.grillaTrabajadores.DataMember = "RECURSOS_HUMANOS";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void limpiarTodo()
        {
            txtApellido.Text = "";
            txtContrasena.Text = "";
            txtNombre.Text = "";
            txtRut.Text = "";
            txtTelefono.Text = "";
            cboCargo.SelectedIndex = 0;
            txtRut.Focus();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient auxNegocioRecursos = new ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient();
                ServiceRecursosHumanos.RecursosHumanos auxRecursos = new ServiceRecursosHumanos.RecursosHumanos();

                DataSet ds = new DataSet();

                ds = auxNegocioRecursos.existeRutTest(txtRut.Text);

                if (validarElimnar() == true)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count < 1)
                        {
                            auxRecursos.RutEmpleado = txtRut.Text;
                            auxRecursos.Nombre = txtNombre.Text;
                            auxRecursos.Apellido = txtApellido.Text;
                            auxRecursos.Telefono = Convert.ToInt32(txtTelefono.Text);
                            auxRecursos.IdCargo = Convert.ToInt32(cboCargo.SelectedValue);
                            auxRecursos.Contrasena = txtContrasena.Text;
                            auxNegocioRecursos.insertarTrabajadorService(auxRecursos);

                            MessageBox.Show("Trabajador  " + txtNombre.Text + "   Registrado Correctamente");
                            actualizarGrilla();
                            limpiarTodo();
                        }
                        else
                        {
                            MessageBox.Show("Rut ya existe", "Error");
                        }
                    }
                    else
                    {
                        MessageBox.Show("No existe la tabla");
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);

            }
        }

        private bool validarElimnar()
        {
            if (txtRut.Text.Length <= 0)
            {
                MessageBox.Show("El campo Rut no puede estar vacio");
                return false;
            }
            else if (txtApellido.Text.Length <= 0)
            {
                MessageBox.Show("El campo Apellido no puede estar vacio");
                return false;
            }
            else if (txtContrasena.Text.Length <= 0)
            {
                MessageBox.Show("El campo Contraseña no puede estar vacio");
                return false;
            }
            else if (txtNombre.Text.Length <= 0)
            {
                MessageBox.Show("El campo Nombre no puede estar vacio");
                return false;
            }
            else if (txtTelefono.Text.Length <= 0)
            {
                MessageBox.Show("El campo Telefono no puede estar vacio");
                return false;
            }
            return true;
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient auxNegocioRecursos = new ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient();
            ServiceRecursosHumanos.RecursosHumanos auxRecursos = new ServiceRecursosHumanos.RecursosHumanos();

            DataSet ds = new DataSet();

            auxRecursos.RutEmpleado = txtRut.Text;

            ds = auxNegocioRecursos.existeRutTest(txtRut.Text);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        if (txtRut.Text.Length >= 1)
                        {

                            //NegocioRecursosHumanos auxNegocioRecursos = new NegocioRecursosHumanos();
                            //RecursosHumanos auxRecursos = new RecursosHumanos();

                            auxRecursos.RutEmpleado = txtRut.Text;

                            auxNegocioRecursos.eliminarTrabajadorService(auxRecursos.RutEmpleado);

                            MessageBox.Show("Trabajador eliminado correctamente " + txtRut.Text);

                            actualizarGrilla();
                            limpiarTodo();
                        }
                        else
                        {
                            MessageBox.Show("El campo RUT no puede estar vacio", "Error");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error", ex.Message);

                    }
                }
                else
                {
                    MessageBox.Show("No existe Trabajador con Rut " + txtRut.Text);
                }
            }
            else
            {
                MessageBox.Show("No existe la tabla");
            }
        }

        private bool validarModificar()
        {
            if (txtApellido.Text.Length == 0)
            {
                MessageBox.Show("El campo Apellido no debe estar vacio", "Error");
                return false;
            }
            else if (txtContrasena.Text.Length == 0)
            {
                MessageBox.Show("El campo Contraseña no debe estar vacio", "Error");
                return false;
            }
            else if (txtNombre.Text.Length == 0)
            {
                MessageBox.Show("El campo Nombre no debe estar vacio", "Error");
                return false;
            }
            else if (txtTelefono.Text.Length == 0)
            {
                MessageBox.Show("El campo Telefono no debe estar vacio", "Error");
                return false;
            }


            return true;

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {

                ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient auxNegocioRecursos = new ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient();
                ServiceRecursosHumanos.RecursosHumanos auxRecursos = new ServiceRecursosHumanos.RecursosHumanos();

                DataSet ds = new DataSet();

                ds = auxNegocioRecursos.existeRutTest(txtRut.Text);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (validarModificar() == true)
                        {
                            auxRecursos.RutEmpleado = txtRut.Text;
                            auxRecursos.Nombre = txtNombre.Text;
                            auxRecursos.Apellido = txtApellido.Text;
                            auxRecursos.Telefono = Convert.ToInt32(txtTelefono.Text);
                            auxRecursos.IdCargo = Convert.ToInt32(cboCargo.SelectedValue);
                            auxRecursos.Contrasena = txtContrasena.Text;
                            auxRecursos.IdCargo = Convert.ToInt32(cboCargo.SelectedValue);

                            auxNegocioRecursos.actualizarTrabajadorService(auxRecursos);

                            MessageBox.Show("Trabajador " + txtNombre.Text + " modificado correctamente");

                            actualizarGrilla();
                            limpiarTodo();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El RUT " + txtRut.Text + " no existe");
                    }
                }
                else
                {
                    MessageBox.Show("Tabla no existe");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }


        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient auxNegocioMantenedor = new ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient();
                ServiceRecursosHumanos.RecursosHumanos auxRecursos = new ServiceRecursosHumanos.RecursosHumanos();

                auxRecursos.RutEmpleado = txtRut.Text;

                auxNegocioMantenedor.retornaTrabajadorPorRutService(auxRecursos);

                grillaTrabajadores.DataSource = auxNegocioMantenedor.retornaTrabajadorPorRutService(auxRecursos);
                this.grillaTrabajadores.DataMember = "Recursos_humanos";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void btnListarTodos_Click(object sender, EventArgs e)
        {
            try
            {
                actualizarGrilla();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            VentanaAdministrador vAdministrador = new VentanaAdministrador();
            this.Hide();
            vAdministrador.ShowDialog();
            this.Close();
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarTodo();
        }
    }
}

