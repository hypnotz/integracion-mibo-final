﻿using CapaDTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaGUI
{
    public partial class VentanaProveedores : Form
    {
        public VentanaProveedores()
        {
            InitializeComponent();
            dgProveedores.AutoGenerateColumns = false;
            txtId.Focus();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            VentanaAdministrador vAdministrador = new VentanaAdministrador();
            this.Hide();
            vAdministrador.ShowDialog();
            this.Close();
        }
        private bool validarCampos()
        {
            if(txtId.Text.Length < 1)
            {
                MessageBox.Show("El campo RUT no puede estar vacio");
                return false;
            }
            else if(txtDireccion.Text.Length < 1)
            {
                MessageBox.Show("El campo DIRECCION no puede estar vacio");
                return false;
            }
            else if(txtEmail.Text.Length < 1)
            {
                MessageBox.Show("El campo EMAIL no puede estar vacio");
                return false;
            }
            else if(txtNombreProveedor.Text.Length < 1)
            {
                MessageBox.Show("El campo NOMBRE no puede estar vacio");
                return false;
            }
            else if (txtTelefono.Text.Length < 1)
            {
                MessageBox.Show("El campo TELEFONO no puede estar vacio");
                return false;
            }

            return true;
        }
        private void btoIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceProveedor.WebServiceProveedorSoapClient auxNegocioProveedor = new ServiceProveedor.WebServiceProveedorSoapClient();
                ServiceProveedor.Proveedor auxProveedor = new ServiceProveedor.Proveedor();

                NegocioProveedor auxNegocioProveedor2 = new NegocioProveedor();
                Proveedor auxProveedor2 = new Proveedor();

                DataSet ds = new DataSet();

                ds = auxNegocioProveedor2.existeRutProveedor(txtId.Text);

                if(ds.Tables.Count > 0)
                {
                    if(ds.Tables[0].Rows.Count < 1)
                    {
                        if(validarCampos() == true)
                        {
                            auxProveedor.RutProveedor = txtId.Text;
                            auxProveedor.NombreProveedor = txtNombreProveedor.Text;
                            auxProveedor.Direccion = txtDireccion.Text;
                            auxProveedor.Telefono = int.Parse(txtTelefono.Text);
                            auxProveedor.Email = txtEmail.Text;
                            auxProveedor.IdComuna = Convert.ToInt32(cboComuna.SelectedValue);


                            auxNegocioProveedor.insertarProveedorService(auxProveedor);

                            MessageBox.Show("Proveedor Ingresado Correctamente");

                            actualizarProveedoresx();
                            LimpiarTodo();
                            
                        }
                    }
                    else
                    {
                        MessageBox.Show("RUT ya existe");
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra la tabla");
                }

               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void VentanaProveedores_Load(object sender, EventArgs e)
        {
           


            cboComuna.DropDownStyle = ComboBoxStyle.DropDownList;


            ServiceComuna.WebServiceComunaSoapClient auxNegocioComuna = new ServiceComuna.WebServiceComunaSoapClient();
            //NegocioComuna auxNegocioComuna = new NegocioComuna();
            cboComuna.DisplayMember = "DESCRIPCION_COMUNA";
            cboComuna.ValueMember = "ID_COMUNA";
            cboComuna.DataSource = auxNegocioComuna.obtenerComunaService();

            actualizarProveedoresx();

        }
        private void actualizarProveedoresx()
        {
            try
            {
                ServiceProveedor.WebServiceProveedorSoapClient auxNegocioProveedor = new ServiceProveedor.WebServiceProveedorSoapClient();
                this.dgProveedores.DataSource = auxNegocioProveedor.retornaProveedorService();
                this.dgProveedores.DataMember = "Proveedor";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ", ex.Message);
            }

        }

        private void LimpiarTodo()
        {
            txtDireccion.Text = "";
            txtEmail.Text = "";
            txtId.Text = "";
            txtNombreProveedor.Text = "";
            txtTelefono.Text = "";
            cboComuna.SelectedValue = 0;
        }

        private void btoEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceProveedor.WebServiceProveedorSoapClient auxNegocioProveedor = new ServiceProveedor.WebServiceProveedorSoapClient();
                ServiceProveedor.Proveedor auxProveedor = new ServiceProveedor.Proveedor();

                NegocioProveedor auxNegocioProveedor2 = new NegocioProveedor();
                Proveedor auxProveedor2 = new Proveedor();

                DataSet ds = new DataSet();

                ds = auxNegocioProveedor2.existeRutProveedor(txtId.Text);

                if(ds.Tables.Count > 0)
                {
                    if(ds.Tables[0].Rows.Count > 0)
                    {
                        if(txtId.Text.Length > 0)
                        {
                            auxProveedor.RutProveedor = txtId.Text;

                            auxNegocioProveedor.eliminarProveedorService(auxProveedor.RutProveedor);

                            MessageBox.Show("Proveedor nro " + txtId.Text + " eliminado correctamente");
                            actualizarProveedoresx();
                            LimpiarTodo();


                        }
                        else
                        {
                            MessageBox.Show("Campo RUT no puede estar vacio");
                        }

                    }
                    else
                    {
                        MessageBox.Show("RUT no existe");
                    }
                }
                else
                {
                    MessageBox.Show("Tabla no existe");
                }

                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ", ex.Message);
            }

        }

        private void btoListar_Click(object sender, EventArgs e)
        {
            actualizarProveedoresx();
        }

        private bool validarModificar()
        {

            if(txtDireccion.Text.Length < 1)
            {
                MessageBox.Show("El Campo direccion no puede estar vacio");
                return false;
            }
            else if(txtEmail.Text.Length < 1)
            {
                MessageBox.Show("El Campo Email no puede estar vacio");
                return false;
            }
            else if(txtId.Text.Length < 1)
            {
                MessageBox.Show("El campo ID no puede estar vacio");
                return false;
            }
            else if(txtNombreProveedor.Text.Length < 1)
            {
                MessageBox.Show("El campo Nombre Proveedor no puede estar vacio");
                return false;
            }
            else if(txtTelefono.Text.Length < 1)
            {
                MessageBox.Show("El campo Telefono no puede estar vacio");
                return false;
            }
          
            

            return true;
        }
        private void btoModificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarModificar() == true)
                {
                    ServiceProveedor.WebServiceProveedorSoapClient auxNegocioProveedor = new ServiceProveedor.WebServiceProveedorSoapClient();
                    ServiceProveedor.Proveedor auxProveedor = new ServiceProveedor.Proveedor();
                    //NegocioProveedor auxNegocioProveedor = new NegocioProveedor();
                    //Proveedor auxProveedor = new Proveedor();

                    auxProveedor.RutProveedor = txtId.Text;
                    auxProveedor.NombreProveedor = txtNombreProveedor.Text;
                    auxProveedor.Direccion = txtDireccion.Text;
                    auxProveedor.Telefono = Convert.ToInt32(txtTelefono.Text);
                    auxProveedor.Email = txtEmail.Text;
                    auxProveedor.IdComuna = Convert.ToInt32(cboComuna.SelectedValue);

                    auxNegocioProveedor.actualizarProveedorService(auxProveedor);

                    MessageBox.Show("Proveedor " + txtNombreProveedor.Text + " actualizado correctamente");

                    actualizarProveedoresx();
                    LimpiarTodo();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }

        }

        private void txtIdProveedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void btnBuscarProveedor_Click(object sender, EventArgs e)
        {
       
            try
            {
                if(txtId.Text.Length > 0)
                {
                    ServiceProveedor.WebServiceProveedorSoapClient auxNegocioProveedor = new ServiceProveedor.WebServiceProveedorSoapClient();
                    ServiceProveedor.Proveedor auxProveedor = new ServiceProveedor.Proveedor();


                    auxProveedor.RutProveedor = txtId.Text;

                    auxNegocioProveedor.retornaProveedorPorRut(auxProveedor);

                    dgProveedores.DataSource = auxNegocioProveedor.retornaProveedorPorRut(auxProveedor);
                    this.dgProveedores.DataMember = "Proveedor";
                    LimpiarTodo();
                }
                else
                {
                    MessageBox.Show("El campo RUT no puede estar vacio");
                }
                
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error " + ex.Message);
            }

        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
    }
}



