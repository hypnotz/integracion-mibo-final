﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUI
{
    public partial class VentanaAdministrador : Form
    {
        public VentanaAdministrador()
        {
            InitializeComponent();
        }

        private void btnRecursosHumanos_Click(object sender, EventArgs e)
        {
            VentanaRecursosHumanos vRecursos = new VentanaRecursosHumanos();
            this.Hide();
            vRecursos.ShowDialog();
            this.Close();
        }

        private void btnProveedor_Click(object sender, EventArgs e)
        {
            VentanaProveedores vProveedor = new VentanaProveedores();
            this.Hide();
            vProveedor.ShowDialog();
            this.Close();
        }

        private void btnCategoria_Click(object sender, EventArgs e)
        {
            VentanaIngresarCategoria vIngresar = new VentanaIngresarCategoria();
            this.Hide();
            vIngresar.ShowDialog();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IniciarSesion vIniciar = new IniciarSesion();
            this.Hide();
            vIniciar.ShowDialog();
            this.Close();
        }

        private void btnIngreso_Click(object sender, EventArgs e)
        {
            VentanaIngresoNuevoProducto vIngreso = new VentanaIngresoNuevoProducto();
            this.Hide();
            vIngreso.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void btnIngresoStock_Click(object sender, EventArgs e)
        {
            VentanaEntradaProducto vEntrada = new VentanaEntradaProducto();
            this.Hide();
            vEntrada.ShowDialog();
            this.Close();
        }

        private void btnIngreso2_Click(object sender, EventArgs e)
        {
            VerStockProductos vIngreso = new VerStockProductos();
            this.Hide();
            vIngreso.ShowDialog();
            this.Close();
            
        }
    }
}
