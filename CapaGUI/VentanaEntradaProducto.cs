﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDTO;
using CapaNegocio;

namespace CapaGUI
{
    public partial class VentanaEntradaProducto : Form
    {
        public VentanaEntradaProducto()
        {
            InitializeComponent();
        }

        private void btoIngresarEntrada_Click(object sender, EventArgs e)
        {
            try
            {
                //Entrada Cabecera Producto//
                ServiceEntradaProducto.WebServiceEntradaProductoSoapClient auxNegocioEntrada = new ServiceEntradaProducto.WebServiceEntradaProductoSoapClient();
                ServiceEntradaProducto.EntradaCabecera auxEntradaCabecera = new ServiceEntradaProducto.EntradaCabecera();

                auxEntradaCabecera.Fecha = DateTime.Today;
                auxEntradaCabecera.IdBodega = Convert.ToInt32(cboNombreBodega.SelectedValue);
                auxEntradaCabecera.IdProveedor = Convert.ToInt32(cboNombreProveedor.SelectedValue);
                auxEntradaCabecera.RutEmpleado = Convert.ToString(cboNombreTrabajador.SelectedValue);

                //NegocioEntradaProducto testing = new NegocioEntradaProducto();

                auxNegocioEntrada.insertarEntradaCabeceraService(auxEntradaCabecera);

                DataTable datatable = new DataTable();

                datatable = auxNegocioEntrada.retornaIdMaximoService();


                NegocioEntradaProductoDetalle auxEntradaDetalle = new NegocioEntradaProductoDetalle();
                EntradaDetalle auxDetalle = new EntradaDetalle();
               
                auxDetalle.Fecha = DateTime.Today;
                auxDetalle.IdProducto = Convert.ToInt32(cboNombreProducto.SelectedValue);
                auxDetalle.Cantidad = Convert.ToInt32(txtCantidad.Text);
                auxDetalle.Precio = Convert.ToInt32(txtPrecio.Text);
                auxDetalle.SubTotal = (Convert.ToInt32(txtPrecio.Text) * Convert.ToInt32(txtCantidad.Text));
                auxDetalle.IdEntrada = Convert.ToInt32(datatable.Rows[0]["ID_ENTRADA"]);
                auxEntradaDetalle.insertarEntradaDetalle(auxDetalle);

                ServiceStockProducto.WebServiceStockProductoSoapClient auxStockProducto = new ServiceStockProducto.WebServiceStockProductoSoapClient();
                ServiceStockProducto.Stock auxStock = new ServiceStockProducto.Stock();


                auxStock.Cantidad = Convert.ToInt32(txtCantidad.Text);
                auxStock.IdProducto = Convert.ToInt32(cboNombreProducto.SelectedValue);
                auxStockProducto.actualizarStock(auxStock);


                MessageBox.Show("Stock producto ingresado correctamente");

            }
            catch (Exception ex)
            {
                MessageBox.Show("erorr", ex.Message);
            }
        }

        private void btoVolver_Click(object sender, EventArgs e)
        {
            VentanaAdministrador vAdmin = new VentanaAdministrador();
            this.Hide();
            vAdmin.ShowDialog();
            this.Close();
        }

        private void VentanaEntradaProducto_Load(object sender, EventArgs e)
        {
            cboNombreBodega.DropDownStyle = ComboBoxStyle.DropDownList;
            cboNombreTrabajador.DropDownStyle = ComboBoxStyle.DropDownList;
            cboNombreProducto.DropDownStyle = ComboBoxStyle.DropDownList;
            cboNombreBodega.DropDownStyle = ComboBoxStyle.DropDownList;
            cboNombreProveedor.DropDownStyle = ComboBoxStyle.DropDownList;
            try
            {

                ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient auxNegocioRecursos = new ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient();
                //NegocioRecursosHumanos auxNegocioRecursos = new NegocioRecursosHumanos();
                cboNombreTrabajador.DisplayMember = "NOMBRE";
                cboNombreTrabajador.ValueMember = "RUT_EMPLEADO";
                cboNombreTrabajador.DataSource = auxNegocioRecursos.obtenerTrabajadorCboService();

                ServiceProveedor.WebServiceProveedorSoapClient auxNegocioProveedor = new ServiceProveedor.WebServiceProveedorSoapClient();
                //NegocioProveedor auxNegocioProveedor= new NegocioProveedor();
                cboNombreProveedor.DisplayMember = "NOMBRE_PROVEEDOR";
                cboNombreProveedor.ValueMember = "ID_PROVEEDOR";
                cboNombreProveedor.DataSource = auxNegocioProveedor.obtenerProveedorCboService();


                ServiceBodega.WebServiceBodegaSoapClient auxNegocioBodega = new ServiceBodega.WebServiceBodegaSoapClient();
                //NegocioBodega auxNegocioBodega = new NegocioBodega();
                cboNombreBodega.DisplayMember = "NOMBRE_BODEGA";
                cboNombreBodega.ValueMember = "ID_BODEGA";
                cboNombreBodega.DataSource = auxNegocioBodega.obtenerBodegasService();

                ServiceProducto.WebServiceProductoSoapClient auxNegocioProducto = new ServiceProducto.WebServiceProductoSoapClient();
                //NegocioProducto auxNegocioProducto = new NegocioProducto();
                cboNombreProducto.DisplayMember = "NOMBRE_PRODUCTO";
                cboNombreProducto.ValueMember = "ID_PRODUCTO";
                cboNombreProducto.DataSource = auxNegocioProducto.obtenerProductosCboService();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void grillaActumulada()
        {

            //Por si alcanzo..

            EntradaDetalle auxEntrada = new EntradaDetalle();
            ServiceEntradaProducto.WebServiceEntradaProductoSoapClient auxNegocioEntrada = new ServiceEntradaProducto.WebServiceEntradaProductoSoapClient();
            ServiceEntradaProducto.EntradaCabecera auxEntradaCabecera = new ServiceEntradaProducto.EntradaCabecera();

            DataTable datatable = new DataTable();

            auxEntrada.IdProducto = Convert.ToInt32(cboNombreProducto.SelectedValue);
            auxEntrada.Fecha = DateTime.Today;
            auxEntrada.Cantidad = Convert.ToInt32(txtCantidad.Text);
            auxEntrada.Precio = Convert.ToInt32(txtPrecio.Text);
            auxEntrada.SubTotal = (Convert.ToInt32(txtPrecio.Text) * Convert.ToInt32(txtCantidad.Text));
            auxEntrada.IdDetalle = 1;

            datatable = auxNegocioEntrada.retornaIdMaximoService();

           
                dgEntrada.Rows.Add(auxEntrada.IdDetalle, auxEntrada.IdProducto, auxEntrada.Fecha,
                                                                   auxEntrada.Cantidad, auxEntrada.Precio, auxEntrada.SubTotal, 1 + Convert.ToInt32(datatable.Rows[0]["ID_ENTRADA"]));

        }
        private void btoMas_Click(object sender, EventArgs e)
        {
            grillaActumulada();
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
    }
}
