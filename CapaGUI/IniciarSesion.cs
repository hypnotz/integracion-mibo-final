﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDTO;
using CapaNegocio;

namespace CapaGUI
{
    public partial class IniciarSesion : Form
    {
        public IniciarSesion()
        {
            InitializeComponent();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                //SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True");


                //SqlDataAdapter sda = new SqlDataAdapter("SELECT RUT_EMPLEADO,ID_CARGO FROM recursos_humanos WHERE rut_empleado='" + txtNombre.Text + "' AND contrasena='" + txtContrasena.Text + "'", con);

                //NegocioRecursosHumanos auxNegocioRecursos = new NegocioRecursosHumanos();

                ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient auxNegocioRecursos = new ServiceRecursosHumanos.WebServiceRecursosHumanosSoapClient();

                DataTable dt = new DataTable();

                dt = auxNegocioRecursos.consultaLogin(txtNombre.Text, txtContrasena.Text);

                //sda.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][1].ToString() == "1")
                    {
                        VentanaAdministrador vAdministrador = new VentanaAdministrador();
                        this.Hide();
                        vAdministrador.ShowDialog();
                        this.Close();
                    }
                    else if (dt.Rows[0][1].ToString() == "2")
                    {
                        VentanaCajero vCajero = new VentanaCajero();
                        this.Hide();
                        vCajero.ShowDialog();
                        this.Close();
                    }
                    else if (dt.Rows[0][1].ToString() == "3")
                    {
                        VentanaReponedor vReponedor = new VentanaReponedor();
                        this.Hide();
                        vReponedor.ShowDialog();
                        this.Close();
                    }
                    else if (dt.Rows[0][1].ToString() == "4")
                    {

                        VentanaFinanzas vFinanzas = new VentanaFinanzas();
                        this.Hide();
                        vFinanzas.ShowDialog();
                        this.Close();
                    }
                }else
                {
                    MessageBox.Show("Usuario o contraseña incorrecta");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erorr ", ex.Message);
            }
         }

        private void button1_Click(object sender, EventArgs e)
        {
            VentanaAdministrador vAdministrador = new VentanaAdministrador();
            this.Hide();
            vAdministrador.ShowDialog();
            this.Close();
         }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    }

