﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDTO;
using CapaNegocio;

namespace CapaGUI
{
    public partial class VentanaIngresoNuevoProducto : Form
    {
        public VentanaIngresoNuevoProducto()
        {
            InitializeComponent();
            dgProductos.AutoGenerateColumns = false;
            
            
        }

        private void btoVolver_Click(object sender, EventArgs e)
        {
            VentanaAdministrador vAdministrador = new VentanaAdministrador();
            this.Hide();
            vAdministrador.ShowDialog();
            this.Close();
        }

        private void btoIngresar_Click(object sender, EventArgs e)
        {
            try
            {


                if (txtNombreProducto.Text.Length < 1)
                {
                    MessageBox.Show("Campo Nombre Producto no puede estar vacio");
                }
                else if (txtPrecio.Text.Length < 1)
                {
                    MessageBox.Show("Campo precio no puede estar vacio");
                }
                else
                {
                   


                    ServiceProducto.WebServiceProductoSoapClient auxNegocioProducto = new ServiceProducto.WebServiceProductoSoapClient();
                    ServiceProducto.Productos auxProducto = new ServiceProducto.Productos();
                    //Productos auxProducto = new Productos();
                    //NegocioProducto auxNegocioProducto = new NegocioProducto();

                    auxProducto.NombreProducto = txtNombreProducto.Text;
                    auxProducto.Precio = Convert.ToInt32(txtPrecio.Text);
                    auxProducto.IdCategoria = Convert.ToInt32(cboCategoria.SelectedValue);

                    auxNegocioProducto.insertarProductoService(auxProducto);
                    MessageBox.Show("Producto Ingresado  Correctamente", "Sistema");
                    actualizarGrillaProducto();

                    

                    //NegocioProducto auxNegocioProductoTest = new NegocioProducto();
                    //Productos auxProductoTest = new Productos();

                    DataTable data = new DataTable();

                    data = auxNegocioProducto.retornaIdMaximoProducto();

                    //MessageBox.Show(Convert.ToString(data.Rows[0]["ID_PRODUCTO"]));

                    //Convert.ToInt32(datatable.Rows[0]["ID_ENTRADA"]);
                    //NegocioStockProducto auxNegocioStock = new NegocioStockProducto();
                    //Stock auxStock = new Stock();

                    ServiceStockProducto.WebServiceStockProductoSoapClient auxNegocioStock = new ServiceStockProducto.WebServiceStockProductoSoapClient();
                    ServiceStockProducto.Stock auxStock = new ServiceStockProducto.Stock();


                    auxStock.IdProducto = Convert.ToInt32(data.Rows[0]["ID_PRODUCTO"]);
                    auxStock.IdBodega = 1;
                    auxStock.Cantidad = 0;

                    auxNegocioStock.insertarProductoStock(auxStock);


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void btoModificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIdProducto.Text.Length < 1)
                {
                    MessageBox.Show("Campo id producto no puede estar vacio");
                }
                else if (txtNombreProducto.Text.Length < 1)
                {
                    MessageBox.Show("Campo Nombre Producto no puede estar vacio");
                }
                else if (txtPrecio.Text.Length < 1)
                {
                    MessageBox.Show("Campo precio no puede estar vacio");
                }
                else
                {
                    ServiceProducto.WebServiceProductoSoapClient auxNegocioProducto = new ServiceProducto.WebServiceProductoSoapClient();
                    ServiceProducto.Productos auxProducto = new ServiceProducto.Productos();
                    //Productos auxProducto = new Productos();
                    //NegocioProducto auxNegocioProducto = new NegocioProducto();


                    auxProducto.IdProducto = Convert.ToInt32(txtIdProducto.Text);
                    auxProducto.NombreProducto = txtNombreProducto.Text;
                    auxProducto.Precio = Convert.ToInt32(txtPrecio.Text);
                    auxProducto.IdCategoria = Convert.ToInt32(cboCategoria.SelectedValue);

                    auxNegocioProducto.actualizarProductoService(auxProducto);

                    MessageBox.Show("Producto actualizado Correctamente", "Sistema");

                    actualizarGrillaProducto();

                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void btoListar_Click(object sender, EventArgs e)
        {
            NegocioProducto auxNegocioProducto = new NegocioProducto();
            Productos auxProducto = new Productos();


            dgProductos.DataSource =  auxNegocioProducto.retornaProductos();
            this.dgProductos.DataMember = "Productos";
        }

        private void actualizarGrillaProducto()
        {
            try
            {
                ServiceProducto.WebServiceProductoSoapClient auxNegocioProducto = new ServiceProducto.WebServiceProductoSoapClient();
                this.dgProductos.DataSource = auxNegocioProducto.retornaProductosService();
                this.dgProductos.DataMember = "Productos";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void VentanaIngresoNuevoProducto_Load(object sender, EventArgs e)
        {
           
            cboCategoria.DropDownStyle = ComboBoxStyle.DropDownList;
            
            try
            {


                ServiceCategoriaa.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoriaa.WebServiceCategoriaSoapClient();
                //NegocioCategoria auxNegocioCategoria = new NegocioCategoria();

                cboCategoria.DisplayMember = "NOMBRE";
                cboCategoria.ValueMember = "ID_CATEGORIA";
                cboCategoria.DataSource = auxNegocioCategoria.obtenerCategoriaService();



                //NegocioProducto auxNegocioProducto = new NegocioProducto();
                ServiceProducto.WebServiceProductoSoapClient auxNegocioProducto = new ServiceProducto.WebServiceProductoSoapClient();
                this.dgProductos.DataSource = auxNegocioProducto.retornaProductosService();
                this.dgProductos.DataMember = "Productos";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void btoEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIdProducto.Text.Length < 1)
                {
                    MessageBox.Show("Campo Id no puede estar vacio");
                }
                else
                {
                    ServiceProducto.WebServiceProductoSoapClient auxNegocioProducto = new ServiceProducto.WebServiceProductoSoapClient();
                    ServiceProducto.Productos auxProducto = new ServiceProducto.Productos();
                    //Productos auxProducto = new Productos();
                    //NegocioProducto auxNegocioProducto = new NegocioProducto();

                    auxProducto.IdProducto = Convert.ToInt32(txtIdProducto.Text);

                    auxNegocioProducto.eliminarProductoService(auxProducto.IdProducto);

                    actualizarGrillaProducto();
                    
                    //auxNegocioProducto.ac;
                    txtIdProducto.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
        }

        private void btnModificarEncima_Click(object sender, EventArgs e)
        {
            lblProducto.Visible = true;
            txtIdProducto.Visible = true;

            
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void txtIdProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(txtIdProducto.Text.Length > 0)
            {
                NegocioProducto auxNegocioProducto = new NegocioProducto();
                Productos auxProducto = new Productos();


                auxProducto.IdProducto = Convert.ToInt32(txtIdProducto.Text);

                auxNegocioProducto.retornaProductoPorID(auxProducto);


                dgProductos.DataSource = auxNegocioProducto.retornaProductoPorID(auxProducto);
                this.dgProductos.DataMember = "Productos";
            }
            else
            {
                MessageBox.Show("El campo ID no puede estar vacio");
            }
           


        }
    }
}


