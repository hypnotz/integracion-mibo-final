﻿namespace CapaGUI
{
    partial class VentanaProveedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.txtNombreProveedor = new System.Windows.Forms.TextBox();
            this.cboComuna = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgProveedores = new System.Windows.Forms.DataGridView();
            this.rut_proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMBRE_PROVEEDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIRECCION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TELEFONO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRIPCION_COMUNA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btoEliminar = new System.Windows.Forms.Button();
            this.btoListar = new System.Windows.Forms.Button();
            this.btoModificar = new System.Windows.Forms.Button();
            this.btoIngresar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.btnBuscarProveedor = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgProveedores)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(569, 407);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(151, 23);
            this.btnVolver.TabIndex = 12;
            this.btnVolver.Text = "Volver al Menú";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(529, 123);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(164, 20);
            this.txtEmail.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(433, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 63;
            this.label6.Text = "Email :";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(529, 36);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(164, 20);
            this.txtTelefono.TabIndex = 4;
            this.txtTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono_KeyPress);
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(165, 127);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(149, 20);
            this.txtDireccion.TabIndex = 3;
            // 
            // txtNombreProveedor
            // 
            this.txtNombreProveedor.Location = new System.Drawing.Point(165, 86);
            this.txtNombreProveedor.Name = "txtNombreProveedor";
            this.txtNombreProveedor.Size = new System.Drawing.Size(149, 20);
            this.txtNombreProveedor.TabIndex = 2;
            // 
            // cboComuna
            // 
            this.cboComuna.FormattingEnabled = true;
            this.cboComuna.Location = new System.Drawing.Point(529, 79);
            this.cboComuna.Name = "cboComuna";
            this.cboComuna.Size = new System.Drawing.Size(164, 21);
            this.cboComuna.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(421, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Comuna :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(421, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 57;
            this.label3.Text = "Teléfono :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 56;
            this.label2.Text = "Dirección :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 55;
            this.label1.Text = "Nombre Proveedor :";
            // 
            // dgProveedores
            // 
            this.dgProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProveedores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rut_proveedor,
            this.NOMBRE_PROVEEDOR,
            this.DIRECCION,
            this.TELEFONO,
            this.EMAIL,
            this.DESCRIPCION_COMUNA});
            this.dgProveedores.Location = new System.Drawing.Point(28, 170);
            this.dgProveedores.Name = "dgProveedores";
            this.dgProveedores.Size = new System.Drawing.Size(785, 170);
            this.dgProveedores.TabIndex = 72;
            // 
            // rut_proveedor
            // 
            this.rut_proveedor.DataPropertyName = "rut_proveedor";
            this.rut_proveedor.HeaderText = "RUT";
            this.rut_proveedor.Name = "rut_proveedor";
            // 
            // NOMBRE_PROVEEDOR
            // 
            this.NOMBRE_PROVEEDOR.DataPropertyName = "NOMBRE_PROVEEDOR";
            this.NOMBRE_PROVEEDOR.HeaderText = "NOMBRE PROVEEDOR";
            this.NOMBRE_PROVEEDOR.Name = "NOMBRE_PROVEEDOR";
            this.NOMBRE_PROVEEDOR.Width = 150;
            // 
            // DIRECCION
            // 
            this.DIRECCION.DataPropertyName = "direccion";
            this.DIRECCION.HeaderText = "DIRECCION";
            this.DIRECCION.Name = "DIRECCION";
            this.DIRECCION.Width = 120;
            // 
            // TELEFONO
            // 
            this.TELEFONO.DataPropertyName = "telefono";
            this.TELEFONO.HeaderText = "TELEFONO";
            this.TELEFONO.Name = "TELEFONO";
            // 
            // EMAIL
            // 
            this.EMAIL.DataPropertyName = "email";
            this.EMAIL.HeaderText = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.Width = 120;
            // 
            // DESCRIPCION_COMUNA
            // 
            this.DESCRIPCION_COMUNA.DataPropertyName = "DESCRIPCION_COMUNA";
            this.DESCRIPCION_COMUNA.HeaderText = "COMUNA";
            this.DESCRIPCION_COMUNA.Name = "DESCRIPCION_COMUNA";
            this.DESCRIPCION_COMUNA.Width = 150;
            // 
            // btoEliminar
            // 
            this.btoEliminar.Location = new System.Drawing.Point(569, 365);
            this.btoEliminar.Name = "btoEliminar";
            this.btoEliminar.Size = new System.Drawing.Size(150, 23);
            this.btoEliminar.TabIndex = 11;
            this.btoEliminar.Text = "Eliminar Proveedor";
            this.btoEliminar.UseVisualStyleBackColor = true;
            this.btoEliminar.Click += new System.EventHandler(this.btoEliminar_Click);
            // 
            // btoListar
            // 
            this.btoListar.Location = new System.Drawing.Point(332, 407);
            this.btoListar.Name = "btoListar";
            this.btoListar.Size = new System.Drawing.Size(150, 23);
            this.btoListar.TabIndex = 10;
            this.btoListar.Text = "Listar todo los Proveedores";
            this.btoListar.UseVisualStyleBackColor = true;
            this.btoListar.Click += new System.EventHandler(this.btoListar_Click);
            // 
            // btoModificar
            // 
            this.btoModificar.Location = new System.Drawing.Point(332, 365);
            this.btoModificar.Name = "btoModificar";
            this.btoModificar.Size = new System.Drawing.Size(150, 23);
            this.btoModificar.TabIndex = 9;
            this.btoModificar.Text = "Modificar Proveedor";
            this.btoModificar.UseVisualStyleBackColor = true;
            this.btoModificar.Click += new System.EventHandler(this.btoModificar_Click);
            // 
            // btoIngresar
            // 
            this.btoIngresar.Location = new System.Drawing.Point(86, 365);
            this.btoIngresar.Name = "btoIngresar";
            this.btoIngresar.Size = new System.Drawing.Size(150, 23);
            this.btoIngresar.TabIndex = 7;
            this.btoIngresar.Text = "Ingresar nuevo Proveedor";
            this.btoIngresar.UseVisualStyleBackColor = true;
            this.btoIngresar.Click += new System.EventHandler(this.btoIngresar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 73;
            this.label5.Text = "Rut Proveedor:";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(165, 44);
            this.txtId.MaxLength = 9;
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(149, 20);
            this.txtId.TabIndex = 1;
            this.txtId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIdProveedor_KeyPress);
            // 
            // btnBuscarProveedor
            // 
            this.btnBuscarProveedor.Location = new System.Drawing.Point(86, 407);
            this.btnBuscarProveedor.Name = "btnBuscarProveedor";
            this.btnBuscarProveedor.Size = new System.Drawing.Size(151, 23);
            this.btnBuscarProveedor.TabIndex = 8;
            this.btnBuscarProveedor.Text = "Buscar Proveedor por RUT";
            this.btnBuscarProveedor.UseVisualStyleBackColor = true;
            this.btnBuscarProveedor.Click += new System.EventHandler(this.btnBuscarProveedor_Click);
            // 
            // VentanaProveedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 467);
            this.Controls.Add(this.btnBuscarProveedor);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dgProveedores);
            this.Controls.Add(this.btoEliminar);
            this.Controls.Add(this.btoListar);
            this.Controls.Add(this.btoModificar);
            this.Controls.Add(this.btoIngresar);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.txtNombreProveedor);
            this.Controls.Add(this.cboComuna);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnVolver);
            this.Name = "VentanaProveedores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentanaProveedores";
            this.Load += new System.EventHandler(this.VentanaProveedores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgProveedores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.TextBox txtNombreProveedor;
        private System.Windows.Forms.ComboBox cboComuna;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgProveedores;
        private System.Windows.Forms.Button btoEliminar;
        private System.Windows.Forms.Button btoListar;
        private System.Windows.Forms.Button btoModificar;
        private System.Windows.Forms.Button btoIngresar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.DataGridViewTextBoxColumn rut_proveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMBRE_PROVEEDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIRECCION;
        private System.Windows.Forms.DataGridViewTextBoxColumn TELEFONO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRIPCION_COMUNA;
        private System.Windows.Forms.Button btnBuscarProveedor;
    }
}