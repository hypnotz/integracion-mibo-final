﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CapaGUI.ServiceProducto {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceProducto.WebServiceProductoSoap")]
    public interface WebServiceProductoSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/insertarProductoService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void insertarProductoService(CapaGUI.ServiceProducto.Productos producto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/insertarProductoService", ReplyAction="*")]
        System.Threading.Tasks.Task insertarProductoServiceAsync(CapaGUI.ServiceProducto.Productos producto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/eliminarProductoService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void eliminarProductoService(int id_Producto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/eliminarProductoService", ReplyAction="*")]
        System.Threading.Tasks.Task eliminarProductoServiceAsync(int id_Producto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/actualizarProductoService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void actualizarProductoService(CapaGUI.ServiceProducto.Productos producto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/actualizarProductoService", ReplyAction="*")]
        System.Threading.Tasks.Task actualizarProductoServiceAsync(CapaGUI.ServiceProducto.Productos producto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaProductosService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet retornaProductosService();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaProductosService", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> retornaProductosServiceAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/obtenerProductosCboService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataTable obtenerProductosCboService();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/obtenerProductosCboService", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataTable> obtenerProductosCboServiceAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaIdMaximoProducto", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataTable retornaIdMaximoProducto();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaIdMaximoProducto", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataTable> retornaIdMaximoProductoAsync();
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Productos : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int idProductoField;
        
        private string nombreProductoField;
        
        private int precioField;
        
        private int idCategoriaField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int IdProducto {
            get {
                return this.idProductoField;
            }
            set {
                this.idProductoField = value;
                this.RaisePropertyChanged("IdProducto");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string NombreProducto {
            get {
                return this.nombreProductoField;
            }
            set {
                this.nombreProductoField = value;
                this.RaisePropertyChanged("NombreProducto");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int Precio {
            get {
                return this.precioField;
            }
            set {
                this.precioField = value;
                this.RaisePropertyChanged("Precio");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public int IdCategoria {
            get {
                return this.idCategoriaField;
            }
            set {
                this.idCategoriaField = value;
                this.RaisePropertyChanged("IdCategoria");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WebServiceProductoSoapChannel : CapaGUI.ServiceProducto.WebServiceProductoSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WebServiceProductoSoapClient : System.ServiceModel.ClientBase<CapaGUI.ServiceProducto.WebServiceProductoSoap>, CapaGUI.ServiceProducto.WebServiceProductoSoap {
        
        public WebServiceProductoSoapClient() {
        }
        
        public WebServiceProductoSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WebServiceProductoSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceProductoSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceProductoSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void insertarProductoService(CapaGUI.ServiceProducto.Productos producto) {
            base.Channel.insertarProductoService(producto);
        }
        
        public System.Threading.Tasks.Task insertarProductoServiceAsync(CapaGUI.ServiceProducto.Productos producto) {
            return base.Channel.insertarProductoServiceAsync(producto);
        }
        
        public void eliminarProductoService(int id_Producto) {
            base.Channel.eliminarProductoService(id_Producto);
        }
        
        public System.Threading.Tasks.Task eliminarProductoServiceAsync(int id_Producto) {
            return base.Channel.eliminarProductoServiceAsync(id_Producto);
        }
        
        public void actualizarProductoService(CapaGUI.ServiceProducto.Productos producto) {
            base.Channel.actualizarProductoService(producto);
        }
        
        public System.Threading.Tasks.Task actualizarProductoServiceAsync(CapaGUI.ServiceProducto.Productos producto) {
            return base.Channel.actualizarProductoServiceAsync(producto);
        }
        
        public System.Data.DataSet retornaProductosService() {
            return base.Channel.retornaProductosService();
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> retornaProductosServiceAsync() {
            return base.Channel.retornaProductosServiceAsync();
        }
        
        public System.Data.DataTable obtenerProductosCboService() {
            return base.Channel.obtenerProductosCboService();
        }
        
        public System.Threading.Tasks.Task<System.Data.DataTable> obtenerProductosCboServiceAsync() {
            return base.Channel.obtenerProductosCboServiceAsync();
        }
        
        public System.Data.DataTable retornaIdMaximoProducto() {
            return base.Channel.retornaIdMaximoProducto();
        }
        
        public System.Threading.Tasks.Task<System.Data.DataTable> retornaIdMaximoProductoAsync() {
            return base.Channel.retornaIdMaximoProductoAsync();
        }
    }
}
