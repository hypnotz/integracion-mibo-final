﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CapaGUI.ServiceProveedor {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceProveedor.WebServiceProveedorSoap")]
    public interface WebServiceProveedorSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/insertarProveedorService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void insertarProveedorService(CapaGUI.ServiceProveedor.Proveedor proveedor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/insertarProveedorService", ReplyAction="*")]
        System.Threading.Tasks.Task insertarProveedorServiceAsync(CapaGUI.ServiceProveedor.Proveedor proveedor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/eliminarProveedorService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void eliminarProveedorService(string rut_proveedor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/eliminarProveedorService", ReplyAction="*")]
        System.Threading.Tasks.Task eliminarProveedorServiceAsync(string rut_proveedor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/actualizarProveedorService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void actualizarProveedorService(CapaGUI.ServiceProveedor.Proveedor proveedor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/actualizarProveedorService", ReplyAction="*")]
        System.Threading.Tasks.Task actualizarProveedorServiceAsync(CapaGUI.ServiceProveedor.Proveedor proveedor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaProveedorService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet retornaProveedorService();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaProveedorService", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> retornaProveedorServiceAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/obtenerProveedorCboService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataTable obtenerProveedorCboService();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/obtenerProveedorCboService", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataTable> obtenerProveedorCboServiceAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaProveedorPorRut", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet retornaProveedorPorRut(CapaGUI.ServiceProveedor.Proveedor proveedor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/retornaProveedorPorRut", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> retornaProveedorPorRutAsync(CapaGUI.ServiceProveedor.Proveedor proveedor);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Proveedor : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int idProveedorField;
        
        private string rutProveedorField;
        
        private string nombreProveedorField;
        
        private string direccionField;
        
        private int telefonoField;
        
        private string emailField;
        
        private int idComunaField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int IdProveedor {
            get {
                return this.idProveedorField;
            }
            set {
                this.idProveedorField = value;
                this.RaisePropertyChanged("IdProveedor");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string RutProveedor {
            get {
                return this.rutProveedorField;
            }
            set {
                this.rutProveedorField = value;
                this.RaisePropertyChanged("RutProveedor");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string NombreProveedor {
            get {
                return this.nombreProveedorField;
            }
            set {
                this.nombreProveedorField = value;
                this.RaisePropertyChanged("NombreProveedor");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string Direccion {
            get {
                return this.direccionField;
            }
            set {
                this.direccionField = value;
                this.RaisePropertyChanged("Direccion");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public int Telefono {
            get {
                return this.telefonoField;
            }
            set {
                this.telefonoField = value;
                this.RaisePropertyChanged("Telefono");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string Email {
            get {
                return this.emailField;
            }
            set {
                this.emailField = value;
                this.RaisePropertyChanged("Email");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public int IdComuna {
            get {
                return this.idComunaField;
            }
            set {
                this.idComunaField = value;
                this.RaisePropertyChanged("IdComuna");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WebServiceProveedorSoapChannel : CapaGUI.ServiceProveedor.WebServiceProveedorSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WebServiceProveedorSoapClient : System.ServiceModel.ClientBase<CapaGUI.ServiceProveedor.WebServiceProveedorSoap>, CapaGUI.ServiceProveedor.WebServiceProveedorSoap {
        
        public WebServiceProveedorSoapClient() {
        }
        
        public WebServiceProveedorSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WebServiceProveedorSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceProveedorSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceProveedorSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void insertarProveedorService(CapaGUI.ServiceProveedor.Proveedor proveedor) {
            base.Channel.insertarProveedorService(proveedor);
        }
        
        public System.Threading.Tasks.Task insertarProveedorServiceAsync(CapaGUI.ServiceProveedor.Proveedor proveedor) {
            return base.Channel.insertarProveedorServiceAsync(proveedor);
        }
        
        public void eliminarProveedorService(string rut_proveedor) {
            base.Channel.eliminarProveedorService(rut_proveedor);
        }
        
        public System.Threading.Tasks.Task eliminarProveedorServiceAsync(string rut_proveedor) {
            return base.Channel.eliminarProveedorServiceAsync(rut_proveedor);
        }
        
        public void actualizarProveedorService(CapaGUI.ServiceProveedor.Proveedor proveedor) {
            base.Channel.actualizarProveedorService(proveedor);
        }
        
        public System.Threading.Tasks.Task actualizarProveedorServiceAsync(CapaGUI.ServiceProveedor.Proveedor proveedor) {
            return base.Channel.actualizarProveedorServiceAsync(proveedor);
        }
        
        public System.Data.DataSet retornaProveedorService() {
            return base.Channel.retornaProveedorService();
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> retornaProveedorServiceAsync() {
            return base.Channel.retornaProveedorServiceAsync();
        }
        
        public System.Data.DataTable obtenerProveedorCboService() {
            return base.Channel.obtenerProveedorCboService();
        }
        
        public System.Threading.Tasks.Task<System.Data.DataTable> obtenerProveedorCboServiceAsync() {
            return base.Channel.obtenerProveedorCboServiceAsync();
        }
        
        public System.Data.DataSet retornaProveedorPorRut(CapaGUI.ServiceProveedor.Proveedor proveedor) {
            return base.Channel.retornaProveedorPorRut(proveedor);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> retornaProveedorPorRutAsync(CapaGUI.ServiceProveedor.Proveedor proveedor) {
            return base.Channel.retornaProveedorPorRutAsync(proveedor);
        }
    }
}
