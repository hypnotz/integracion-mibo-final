﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CapaGUI.ServiceBodega {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceBodega.WebServiceBodegaSoap")]
    public interface WebServiceBodegaSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/obtenerBodegasService", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataTable obtenerBodegasService();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/obtenerBodegasService", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataTable> obtenerBodegasServiceAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WebServiceBodegaSoapChannel : CapaGUI.ServiceBodega.WebServiceBodegaSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WebServiceBodegaSoapClient : System.ServiceModel.ClientBase<CapaGUI.ServiceBodega.WebServiceBodegaSoap>, CapaGUI.ServiceBodega.WebServiceBodegaSoap {
        
        public WebServiceBodegaSoapClient() {
        }
        
        public WebServiceBodegaSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WebServiceBodegaSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceBodegaSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceBodegaSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Data.DataTable obtenerBodegasService() {
            return base.Channel.obtenerBodegasService();
        }
        
        public System.Threading.Tasks.Task<System.Data.DataTable> obtenerBodegasServiceAsync() {
            return base.Channel.obtenerBodegasServiceAsync();
        }
    }
}
