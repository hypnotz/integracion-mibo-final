﻿namespace CapaGUI
{
    partial class VentanaAdministrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRecursosHumanos = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnProveedor = new System.Windows.Forms.Button();
            this.btnCategoria = new System.Windows.Forms.Button();
            this.btnIngreso = new System.Windows.Forms.Button();
            this.btnIngresoStock = new System.Windows.Forms.Button();
            this.btnIngreso2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRecursosHumanos
            // 
            this.btnRecursosHumanos.Location = new System.Drawing.Point(190, 69);
            this.btnRecursosHumanos.Name = "btnRecursosHumanos";
            this.btnRecursosHumanos.Size = new System.Drawing.Size(314, 23);
            this.btnRecursosHumanos.TabIndex = 0;
            this.btnRecursosHumanos.Text = "Ventana Recursos Humanos";
            this.btnRecursosHumanos.UseVisualStyleBackColor = true;
            this.btnRecursosHumanos.Click += new System.EventHandler(this.btnRecursosHumanos_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(496, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Cerrar Sesion";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnProveedor
            // 
            this.btnProveedor.Location = new System.Drawing.Point(190, 120);
            this.btnProveedor.Name = "btnProveedor";
            this.btnProveedor.Size = new System.Drawing.Size(314, 23);
            this.btnProveedor.TabIndex = 2;
            this.btnProveedor.Text = "Ventana Proveedores";
            this.btnProveedor.UseVisualStyleBackColor = true;
            this.btnProveedor.Click += new System.EventHandler(this.btnProveedor_Click);
            // 
            // btnCategoria
            // 
            this.btnCategoria.Location = new System.Drawing.Point(190, 174);
            this.btnCategoria.Name = "btnCategoria";
            this.btnCategoria.Size = new System.Drawing.Size(314, 23);
            this.btnCategoria.TabIndex = 0;
            this.btnCategoria.Text = "Ventana Ingresar Categoria";
            this.btnCategoria.Click += new System.EventHandler(this.btnCategoria_Click);
            // 
            // btnIngreso
            // 
            this.btnIngreso.Location = new System.Drawing.Point(190, 222);
            this.btnIngreso.Name = "btnIngreso";
            this.btnIngreso.Size = new System.Drawing.Size(314, 23);
            this.btnIngreso.TabIndex = 3;
            this.btnIngreso.Text = "Ventana Nuevo Producto";
            this.btnIngreso.UseVisualStyleBackColor = true;
            this.btnIngreso.Click += new System.EventHandler(this.btnIngreso_Click);
            // 
            // btnIngresoStock
            // 
            this.btnIngresoStock.Location = new System.Drawing.Point(190, 274);
            this.btnIngresoStock.Name = "btnIngresoStock";
            this.btnIngresoStock.Size = new System.Drawing.Size(314, 23);
            this.btnIngresoStock.TabIndex = 4;
            this.btnIngresoStock.Text = "Ventana Ingreso Stock Producto";
            this.btnIngresoStock.UseVisualStyleBackColor = true;
            this.btnIngresoStock.Click += new System.EventHandler(this.btnIngresoStock_Click);
            // 
            // btnIngreso2
            // 
            this.btnIngreso2.Location = new System.Drawing.Point(190, 329);
            this.btnIngreso2.Name = "btnIngreso2";
            this.btnIngreso2.Size = new System.Drawing.Size(314, 20);
            this.btnIngreso2.TabIndex = 5;
            this.btnIngreso2.Text = "Ver Stock Productos";
            this.btnIngreso2.UseVisualStyleBackColor = true;
            this.btnIngreso2.Click += new System.EventHandler(this.btnIngreso2_Click);
            // 
            // VentanaAdministrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 469);
            this.Controls.Add(this.btnIngreso2);
            this.Controls.Add(this.btnIngresoStock);
            this.Controls.Add(this.btnIngreso);
            this.Controls.Add(this.btnCategoria);
            this.Controls.Add(this.btnProveedor);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRecursosHumanos);
            this.Name = "VentanaAdministrador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrador";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRecursosHumanos;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnProveedor;
        private System.Windows.Forms.Button btnCategoria;
        private System.Windows.Forms.Button btnIngreso;
        private System.Windows.Forms.Button btnIngresoStock;
        private System.Windows.Forms.Button btnIngreso2;
    }
}