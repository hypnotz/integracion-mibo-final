﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDTO;

namespace CapaGUI
{
    public partial class VentanaIngresarCategoria : Form
    {
        public VentanaIngresarCategoria()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
        }

        private bool validarCampos()
        {
            if(txtNombreCategoria.Text.Length < 1)
            {
                MessageBox.Show("El Campo nombre no puede estar vacio");
                return false;
            }
            else if(txtDescripcionCategoria.Text.Length < 1)
            {
                MessageBox.Show("El Campo descripcion categoria no puede estar vacio");
                return false;
            }
            return true;
        }

        private bool validarCamposActualizar()
        {
            if (txtNombreCategoria.Text.Length < 1)
            {
                MessageBox.Show("El Campo nombre no puede estar vacio");
                return false;
            }
            else if (txtDescripcionCategoria.Text.Length < 1)
            {
                MessageBox.Show("El Campo descripcion categoria no puede estar vacio");
                return false;
            }else if(txtID.Text.Length < 1)
            {
                MessageBox.Show("El campo ID no puede estar vacio");
                return false;
            }
            return true;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {

            try
            {
               
                if(validarCampos() == true)
                {
                    ServiceCategoriaa.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoriaa.WebServiceCategoriaSoapClient();
                    ServiceCategoriaa.Categoria auxCategoria = new ServiceCategoriaa.Categoria();

                    auxCategoria.Nombre = txtNombreCategoria.Text;
                    auxCategoria.DescripcionCategoria = txtDescripcionCategoria.Text;

                    auxNegocioCategoria.insertarCategoriaService(auxCategoria);

                    MessageBox.Show("Categoria " + txtNombreCategoria.Text + " ingresada correctamente");

                    actualizarTodo();
                }      
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }

        }

        private void limpiar()
        {
            txtDescripcionCategoria.Text = "";
            txtID.Text = "";
            txtNombreCategoria.Text = "";
            txtNombreCategoria.Focus();
        }
        private void VentanaIngresarCategoria_Load(object sender, EventArgs e)
        {
            actualizarTodo();

        }

        private void actualizarTodo()
        {
            try
            {
                ServiceCategoriaa.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoriaa.WebServiceCategoriaSoapClient();
                this.dataGridView1.DataSource = auxNegocioCategoria.retornaCategoriaService();
                this.dataGridView1.DataMember = "Categoria";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarCamposActualizar() == true)
                {
                    ServiceCategoriaa.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoriaa.WebServiceCategoriaSoapClient();
                    ServiceCategoriaa.Categoria auxCategoria = new ServiceCategoriaa.Categoria();

                    auxCategoria.Nombre = txtNombreCategoria.Text;
                    auxCategoria.DescripcionCategoria = txtDescripcionCategoria.Text;
                    auxCategoria.IdCategoria = Convert.ToInt16(txtID.Text);

                    auxNegocioCategoria.actualizarProductoService(auxCategoria);

                    MessageBox.Show("Categoria " + txtID.Text + " actualizada correctamente");

                    actualizarTodo();
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtID.Text.Length >= 1){
                    ServiceCategoriaa.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoriaa.WebServiceCategoriaSoapClient();
                    ServiceCategoriaa.Categoria auxCategoria = new ServiceCategoriaa.Categoria();

                    auxCategoria.IdCategoria = Convert.ToInt16(txtID.Text);

                    auxNegocioCategoria.eliminarCategoriaService(auxCategoria.IdCategoria);

                    MessageBox.Show("Categoria " + txtID.Text + " eliminada correctamente");

                    actualizarTodo();
                }
                else
                {
                    MessageBox.Show("Debe Ingresar el ID para poder eliminar");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VentanaAdministrador vAdministrador = new VentanaAdministrador();
            this.Hide();
            vAdministrador.ShowDialog();
            this.Close();
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsControl(e.KeyChar) || Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            actualizarTodo();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            NegocioCategoria auxNegocioCategoria = new NegocioCategoria();
            Categoria auxCategoria = new Categoria();


            auxCategoria.Nombre = txtNombreCategoria.Text;

            auxNegocioCategoria.retornaCategoriaPorNombre(auxCategoria);

            dataGridView1.DataSource = auxNegocioCategoria.retornaCategoriaPorNombre(auxCategoria);
            this.dataGridView1.DataMember = "Categoria";
        }
    }
}


