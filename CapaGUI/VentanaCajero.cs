﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUI
{
    public partial class VentanaCajero : Form
    {
        public VentanaCajero()
        {
            InitializeComponent();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            IniciarSesion vInciar = new IniciarSesion();
            this.Hide();
            vInciar.ShowDialog();
            this.Close();
        }
    }
}
