﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioRecursosHumanos
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "BD_ALMACEN";
            this.Conec1.NombreTabla = "RECURSOS_HUMANOS";
            this.Conec1.CadenaConexion = @"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True";
        }

        public void insertarTrabajador(RecursosHumanos recursosHumanos)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO " + this.Conec1.NombreTabla
                                    + " (rut_empleado,nombre,apellido,telefono,id_cargo,contrasena) VALUES ('" + recursosHumanos.RutEmpleado +
                                    "','" + recursosHumanos.Nombre + "','" + recursosHumanos.Apellido + "'," + recursosHumanos.Telefono
                                    + "," + recursosHumanos.IdCargo + ",'" + recursosHumanos.Contrasena + "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void actualizarTrabajador(RecursosHumanos recursosHumanos)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE " + this.Conec1.NombreTabla
                                    + " SET" + " NOMBRE = '" + recursosHumanos.Nombre
                                    + "'," + " APELLIDO = '" + recursosHumanos.Apellido
                                    + "'," + " CONTRASENA = '" + recursosHumanos.Contrasena
                                    + "'," + " TELEFONO = " + recursosHumanos.Telefono
                                    + "," + " ID_CARGO = " + recursosHumanos.IdCargo
                                    + " WHERE RUT_EMPLEADO = '" + recursosHumanos.RutEmpleado + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }
        public void eliminarTrabajador(String rut_empleado)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM " + this.Conec1.NombreTabla
                                    + " WHERE RUT_EMPLEADO = '" + rut_empleado + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public DataSet retornaTrabajador()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT h.RUT_EMPLEADO,h.NOMBRE,h.APELLIDO,h.TELEFONO,h.CONTRASENA, ca.NOMBRE as Cargo FROM RECURSOS_HUMANOS h inner join CARGO ca on (ca.ID_CARGO= h.ID_CARGO)";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }
        public DataTable obtenerTrabajadorCbo()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet.Tables["RECURSOS_HUMANOS"];
        }
        public DataSet retornaTrabajadorPorRut(RecursosHumanos recursosHumanos)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla + " WHERE RUT_EMPLEADO = '" + recursosHumanos.RutEmpleado + "';"; ;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataSet existeRutTest(String rut)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT RUT_EMPLEADO FROM " + this.Conec1.NombreTabla + " WHERE RUT_EMPLEADO = '" + rut + "';";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataTable consultaLogin(String rut_empleado, String contrasena)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT RUT_EMPLEADO, ID_CARGO FROM recursos_humanos WHERE rut_empleado = '" + rut_empleado + "' AND contrasena = '" + contrasena + "'";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet.Tables["RECURSOS_HUMANOS"];
        }
    }
}
