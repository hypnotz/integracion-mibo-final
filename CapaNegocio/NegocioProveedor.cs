﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioProveedor
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "BD_ALMACEN";
            this.Conec1.NombreTabla = "PROVEEDOR";
            this.Conec1.CadenaConexion = @"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True";
        }

        public void insertarProveedor(Proveedor proveedor)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO " + this.Conec1.NombreTabla
                                    + " (rut_proveedor,nombre_proveedor,direccion,email,telefono,id_comuna) VALUES ('" + proveedor.RutProveedor + "','" 
                                    + proveedor.NombreProveedor +
                                    "','" + proveedor.Direccion + "','" + proveedor.Email + "'," + proveedor.Telefono + "," + proveedor.IdComuna + ");";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void eliminarProveedor(String rut_proveedor)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM " + this.Conec1.NombreTabla
                                    + " WHERE RUT_PROVEEDOR = '" + rut_proveedor + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void actualizarProveedor(Proveedor proveedor)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE " + this.Conec1.NombreTabla
                                    + " SET " + " NOMBRE_PROVEEDOR = '" + proveedor.NombreProveedor
                                    + "'," + " DIRECCION = '" + proveedor.Direccion
                                    + "'," + "EMAIL = '" + proveedor.Email
                                    + "'," + " TELEFONO = " + proveedor.Telefono
                                    + " WHERE RUT_PROVEEDOR = '" + proveedor.RutProveedor + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public DataSet retornaProveedor()
        {
           
            this.configurarConexion();
            this.Conec1.CadenaSQL = "select po.rut_proveedor, po.nombre_proveedor, po.direccion, po.telefono,po.email, ca.DESCRIPCION_COMUNA from PROVEEDOR po inner join COMUNA ca on(ca.ID_COMUNA = po.ID_COMUNA);";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataTable obtenerProveedorCbo()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet.Tables["PROVEEDOR"];
        }

        public DataSet retornaProveedorPorRut(Proveedor proveedor)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT po.rut_proveedor, po.nombre_proveedor, po.direccion, po.telefono,po.email,ca.DESCRIPCION_COMUNA from PROVEEDOR po inner join COMUNA ca on(ca.ID_COMUNA = po.ID_COMUNA)" + " WHERE rut_proveedor = " + proveedor.RutProveedor + ";";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataSet existeRutProveedor(String rut)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT RUT_PROVEEDOR FROM " + this.Conec1.NombreTabla + " WHERE RUT_PROVEEDOR = '" + rut + "';";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }


        //} //Fin Clase
    } //namespace

    //"SELECT rut_proveedor,nombre_proveedor,direccion,telefono,email,id_comuna FROM " + this.Conec1.NombreTabla + " WHERE RUT_PROVEEDOR = '" + proveedor.RutProveedor + "';"; ;
}

