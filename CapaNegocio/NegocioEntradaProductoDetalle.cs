﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioEntradaProductoDetalle
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "BD_ALMACEN";
            this.Conec1.NombreTabla = "ENTRADA_DETALLE";
            this.Conec1.CadenaConexion = @"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True";
        }

        public void insertarEntradaDetalle(EntradaDetalle entradaDetalle)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO " + this.Conec1.NombreTabla
                                    + " (id_producto,fecha,cantidad_ingresada,precio,sub_total,id_entrada) VALUES ('"
                                    + entradaDetalle.IdProducto + "','" + entradaDetalle.Fecha.ToString("yyyyMMdd") + "','" + entradaDetalle.Cantidad + "','" + entradaDetalle.Precio +
                                    "','" + entradaDetalle.SubTotal + "','" + entradaDetalle.IdEntrada +
                                      "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }
    }

}
