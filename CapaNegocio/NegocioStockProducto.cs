﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioStockProducto
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "BD_ALMACEN";
            this.Conec1.NombreTabla = "STOCK";
            this.Conec1.CadenaConexion = @"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True";
        }

        public DataSet retornaStockGeneral()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT po.ID_PRODUCTO,po.NOMBRE_PRODUCTO,st.CANTIDAD from PRODUCTOS po left join STOCK st on(st.ID_PRODUCTO = po.ID_PRODUCTO)";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataSet retornaStockPorID(Stock stock)
        {
            this.configurarConexion();

            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla + " WHERE ID_PRODUCTO = " + stock.IdProducto;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public void actualizarStock(Stock nuevoStock)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE " + this.Conec1.NombreTabla
                                    + " SET" + " CANTIDAD = CANTIDAD + " + nuevoStock.Cantidad
                                    + " WHERE ID_PRODUCTO = " + nuevoStock.IdProducto + ";";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void insertarProductoStock(Stock nuevoStock)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO " + this.Conec1.NombreTabla
                                    + " (id_producto,id_bodega,cantidad) VALUES (" + nuevoStock.IdProducto +
                                     "," + nuevoStock.IdBodega + "," + nuevoStock.Cantidad + ");";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public DataSet retornaStockProductoPorID(Stock stock)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT po.ID_PRODUCTO,po.NOMBRE_PRODUCTO,st.CANTIDAD from PRODUCTOS po left join STOCK st on(st.ID_PRODUCTO = po.ID_PRODUCTO) where po.ID_PRODUCTO = " + stock.IdProducto + ";";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataSet retornaStockProductoPorNombre(Productos productos)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT po.ID_PRODUCTO,po.NOMBRE_PRODUCTO,st.CANTIDAD from PRODUCTOS po left join STOCK st on(st.ID_PRODUCTO = po.ID_PRODUCTO) where po.NOMBRE_PRODUCTO LIKE '" +"%"+ productos.NombreProducto + "%" +"';";

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }
    }
}

