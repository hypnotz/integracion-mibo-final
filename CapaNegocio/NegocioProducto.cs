﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioProducto
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "BD_ALMACEN";
            this.Conec1.NombreTabla = "PRODUCTOS";
            this.Conec1.CadenaConexion = @"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True";
        }

        public void insertarProducto(Productos producto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO " + this.Conec1.NombreTabla
                                    + " (nombre_producto,precio,id_categoria) VALUES ('" + producto.NombreProducto + "',"
                                    + producto.Precio + "," + producto.IdCategoria + ");";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }
        public void eliminarProducto(int id_Producto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM " + this.Conec1.NombreTabla
                                    + " WHERE id_producto = '" + id_Producto + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void actualizarProducto(Productos producto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE " + this.Conec1.NombreTabla
                                    + " SET " + " NOMBRE_PRODUCTO = '" + producto.NombreProducto
                                    + "'," + " PRECIO = " + producto.Precio
                                    + "," + " ID_CATEGORIA = " + producto.IdCategoria
                                    + " WHERE ID_PRODUCTO = " + producto.IdProducto + ";";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public DataSet retornaProductos()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataTable obtenerProductosCbo()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet.Tables["Productos"];
        }

        public DataTable retornaIdMaximoProducto()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT max(id_producto) as ID_PRODUCTO FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet.Tables["PRODUCTOS"];
        }

        public DataSet retornaProductoPorID(Productos producto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla + " WHERE ID_PRODUCTO = " + producto.IdProducto + ";"; ;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }
    }
}
