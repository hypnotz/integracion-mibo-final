﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioEntradaProducto
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "BD_ALMACEN";
            this.Conec1.NombreTabla = "ENTRADA_CABECERA";
            this.Conec1.CadenaConexion = @"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True";
        }

        public DataTable retornaIdMaximo()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT max(id_entrada) as ID_ENTRADA FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet.Tables["ENTRADA_CABECERA"];
        }

        public void insertarEntradaCabecera(EntradaCabecera entradaCabecera)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO " + this.Conec1.NombreTabla
                                    + " (fecha,id_bodega,id_proveedor,rut_empleado) VALUES ('"
                                    + entradaCabecera.Fecha.ToString("yyyyMMdd") + "'," + entradaCabecera.IdBodega + "," + entradaCabecera.IdProveedor + ",'" +
                                    entradaCabecera.RutEmpleado +
                                      "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }
    }
}

