﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioCategoria
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "BD_ALMACEN";
            this.Conec1.NombreTabla = "CATEGORIA";
            this.Conec1.CadenaConexion = @"Data Source=DESKTOP-45GLS3S\IVAN;Initial Catalog=BD_Almacen;Integrated Security=True";
        }

        public DataTable obtenerCategoria()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet.Tables["Categoria"];
        }

        public void insertarCategoria(Categoria categoria)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO " + this.Conec1.NombreTabla
                                    + " (nombre,descripcion_categoria) VALUES ('" + categoria.Nombre + "','"
                                    + categoria.DescripcionCategoria + "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }
        public void eliminarCategoria(int id_categoria)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM " + this.Conec1.NombreTabla
                                    + " WHERE id_categoria = '" + id_categoria + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void actualizarProducto(Categoria categoria)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE " + this.Conec1.NombreTabla
                                    + " SET " + " nombre = '" + categoria.Nombre
                                    + "'," + " descripcion_categoria = '" + categoria.DescripcionCategoria
                                    + "' WHERE id_categoria = " + categoria.IdCategoria + ";";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public DataSet retornaCategoria()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }

        public DataSet retornaCategoriaPorNombre(Categoria categoria)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla + " WHERE NOMBRE = '" + categoria.Nombre + "';"; ;

            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }
    }
}
